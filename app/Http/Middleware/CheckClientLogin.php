<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Exception;
use App\Http\Library\JwtExt;
use Illuminate\Support\Facades\Cache;

class CheckClientLogin
{
    use JwtExt;
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws Exception
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization') ? trim($request->header('Authorization')) : $request->token;
        if (strlen($token)) {
            $dataDecode = $this->decodeJwt($token, true);
            $dataUser = $dataDecode['data'];
            if (empty($dataUser)) {
                throw new Exception("Không tìm thấy tài khoản.", 999);
            }
            $encryptedKeys = md5($token);
            $id = $dataUser['code'];
            $cacheDataUser = Cache::get("user_info_login:{$id}:{$encryptedKeys}");
            if (!isset($cacheDataUser)) {
                throw new Exception("Không tìm thấy tài khoản.", 999);
            }
            if ($cacheDataUser['type'] !== User::TYPE_CLIENT) {
                throw new Exception('Không có quyền truy cập hệ thống.', 999);
            }
            app()->instance('userData', $cacheDataUser);
            return $next($request);
        }
        throw new Exception("Không tìm thấy phiên đăng nhập.", 999);
    }
}
