<?php

namespace App\Http\Controllers;

use App\Http\Library\ApiResponse;
use App\Models\BaseModel;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ControllerBase extends Controller
{
    protected $response;
    protected $request;

    public function __construct(ApiResponse $response, Request $request)
    {
        $this->response = $response;
        $this->request = $request;
    }

    /**
     * @return Application|mixed
     */
    public function getUserData()
    {
        return app('userData');
    }

    /**
     * @return string
     */
    public function generateCode()
    {
        return Str::random(BaseModel::LENGTH_CODE);
    }

    /**
     * @param $table
     * @param $column
     * @param $value
     * @return boolean
     */
    public function checkExists($table,$column, $value)
    {
        return DB::table($table)->where($column, $value)->exists();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function checkImages($data)
    {
        foreach ($data as $item) {
            if (!empty($item->image) && file_exists('upload/image/' . $item->image)) {
                $item->image = asset('upload/image/' . $item->image);
            } else {
                $item->image = asset(BaseModel::DEFAULT_IMAGE);
            }
        }
        return $data;
    }

    /**
     * @param $item
     * @return mixed
     */
    public function checkImage($item)
    {
        if (!empty($item->image) && file_exists('upload/image/' . $item->image)) {
            $item->image = asset('upload/image/' . $item->image);
        } else {
            $item->image = asset(BaseModel::DEFAULT_IMAGE);
        }
        return $item;
    }

    /**
     * @param $table
     * @param $column
     * @param $value
     * @return mixed|null
     */
    public function getImage($table, $column, $value)
    {
        return DB::table($table)->where($column, $value)->value('image');
    }
}
