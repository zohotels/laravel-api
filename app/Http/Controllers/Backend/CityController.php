<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\TableList;
use Illuminate\Http\Request;
use App\Http\Requests\Backend\CityCreate;
use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Validator;
use Hash;
use DB;

class CityController extends ControllerBase
{
    private $column = ['id','image','name','votes'];

    public function list(TableList $request){
        $limit = !empty($request->get('limit')) ? $request->get('limit') : 10;
        $name = $request->get('name');
        $data = DB::table('city')
        ->where('name','like','%'.$name.'%')
        ->orderBy('id','DESC')
        ->paginate($limit,$this->column);
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$this->checkImages($data));
    }

    public function detail(Request $request){
        $id = $request->get('id');
        if($this->checkExists('city','id',$id)) {
            $data = DB::table('city')
                ->where('id', $id)
                ->first($this->column);
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $this->checkImage($data));
        }
        return $this->response->fail('Mã không tồn tại');
    }
}
