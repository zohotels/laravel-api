<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use App\Http\Requests\Backend\RoomCreate;
use App\Http\Requests\Backend\RoomDelete;
use App\Http\Requests\Backend\RoomUpdate;
use App\Http\Requests\Backend\TableList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoomController extends ControllerBase
{
    private $column = ['room.code', 'hotel.name as hotel_name', 'room.image', 'room.name', 'room.description', 'room.amount', 'room.bed', 'bedtype.name as bed_type', 'room.price', 'room.promotion', 'room.created_at', 'room.updated_at'];

    public function list(TableList $request)
    {
        $limit = !empty($request->get('limit')) ? $request->get('limit') : 10;
        $hotel_code = $request->get('hotel_code');
        $name = $request->get('name');
        $hotel_name = $request->get('hotel_name');
        $bedtype_name = $request->get('bedtype_name');
        $data = DB::table('room')
            ->leftJoin('hotel', 'room.hotelID', 'hotel.id')
            ->leftJoin('bedtype', 'room.typeID', 'bedtype.id')
            ->where([
                ['hotel.code', 'like', '%' . $hotel_code . '%'],
                ['room.name', 'like', '%' . $name . '%'],
                ['hotel.name', 'like', '%' . $hotel_name . '%'],
                ['bedtype.name', 'like', '%' . $bedtype_name . '%'],
            ])
            ->orderBy('room.id', 'DESC')
            ->paginate($limit, $this->column);
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $this->checkImages($data));
    }

    public function detail(Request $request)
    {
        $code = $request->get('code');
        if ($this->checkExists('room','code', $code)) {
            $data = DB::table('room')
                ->leftJoin('hotel', 'room.hotelID', 'hotel.id')
                ->leftJoin('bedtype', 'room.typeID', 'bedtype.id')
                ->where('room.code', $code)
                ->first($this->column);
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $this->checkImage($data));
        }
        return $this->response->fail('Mã không tồn tại');
    }

    public function create(RoomCreate $request)
    {
        $hotelID = $request->get('hotelID');
        $image = time();
        $file = $request->file('image');
        $file->move('upload/image/', $image);
        $name = $request->get('name');
        $description = $request->get('description');
        $amount = $request->get('amount');
        $bed = $request->get('bed');
        $typeID = $request->get('typeID');
        $price = $request->get('price');
        $promotion = $request->get('promotion');
        DB::table('room')->insert([
            'code' => $this->generateCode(),
            'hotelID' => $hotelID,
            'image' => $image,
            'name' => $name,
            'description' => $description,
            'amount' => $amount,
            'bed' => $bed,
            'typeID' => $typeID,
            'price' => $price,
            'promotion' => $promotion
        ]);
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }

    public function update(RoomUpdate $request){
        $code = $request->get('code');
        $hotelID = $request->get('hotelID');
        $image = $this->getImage('room','code',$code);
        if ($request->file('image')) {
            if (file_exists('upload/image/' . $image) && $image != "") {
                unlink('upload/image/' . $image);
            }
            $image = time();
            $file = $request->file('image');
            $file->move('upload/image/', $image);
        }
        $name = $request->get('name');
        $description = $request->get('description');
        $amount = $request->get('amount');
        $bed = $request->get('bed');
        $typeID = $request->get('typeID');
        $price = $request->get('price');
        $promotion = $request->get('promotion');
        DB::table('room')->where('code',$code)->update([
            'hotelID' => $hotelID,
            'image' => $image,
            'name' => $name,
            'description' => $description,
            'amount' => $amount,
            'bed' => $bed,
            'typeID' => $typeID,
            'price' => $price,
            'promotion' => $promotion
        ]);
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }

    public function delete(RoomDelete $request){
        $codes = $request->get('code');
        $codes = array_unique($codes);
        foreach ($codes as $code) {
            $image = DB::table('room')->where('code', $code)->value('image');
            if (file_exists('upload/image/' . $image) && $image != "") {
                unlink('upload/image/' . $image);
            }
            DB::table('room')->where('code', $code)->delete();
        }
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }
}
