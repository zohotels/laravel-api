<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use App\Http\Requests\Backend\ServiceCreate;
use App\Http\Requests\Backend\ServiceDelete;
use App\Http\Requests\Backend\ServiceUpdate;
use App\Http\Requests\Backend\TableList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServiceController extends ControllerBase
{
    private $column = ['id','image','name','description','created_at','updated_at'];

    public function list(TableList $request){
        $limit = !empty($request->get('limit')) ? $request->get('limit') : 10;
        $name = $request->get('name');
        $data = DB::table('service')
            ->where('name','like','%'.$name.'%')
            ->orderBy('id','DESC')
            ->paginate($limit,$this->column);
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$this->checkImages($data));
    }

    public function detail(Request $request){
        $id = $request->get('id');
        if($this->checkExists('service','id',$id)){
            $data = DB::table('service')
                ->where('id',$id)
                ->first($this->column);
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$this->checkImage($data));
        }
        return $this->response->fail('ID không tồn tại');
    }

    public function create(ServiceCreate $request){
        $image = time();
        $file = $request->file('image');$file->move('upload/image',$image);
        $name = $request->get('name');
        $description = $request->get('description');
        DB::table('service')->insert([
            'image' => $image,
            'name' => $name,
            'description' => $description
        ]);
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }

    public function update(ServiceUpdate $request){
        $id = $request->get('id');
        $image = DB::table('service')->where('id',$id)->value('image');
        if($request->file('image')){
            if (file_exists('upload/image/' . $image) && $image != "") {
                unlink('upload/image/' . $image);
            }
            $image = time();
            $file = $request->file('image');
            $file->move('upload/image/', $image);
        }
        $name = $request->get('name');
        $description = $request->get('description');
        DB::table('service')->where('id',$id)->update([
            'image' => $image,
            'name' => $name,
            'description' =>$description
        ]);
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }

    public function delete(ServiceDelete $request){
        $ids = $request->get('id');
        $ids = array_unique($ids);
        foreach ($ids as $id) {
            $image = DB::table('service')->where('id', $id)->value('image');
            if (file_exists('upload/image/' . $image) && $image != "") {
                unlink('upload/image/' . $image);
            }
            DB::table('service')->where('id', $id)->delete();
        }
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }
}
