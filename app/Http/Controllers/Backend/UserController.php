<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\TableList;
use Illuminate\Http\Request;
use App\Http\Requests\Backend\UserCreate;
use App\Http\Requests\Backend\UserUpdate;
use App\Http\Requests\Backend\UserDelete;
use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends ControllerBase
{
    private $column = ['code', 'fullname', 'phone', 'avatar', 'email', 'address', 'introduce_code', 'type', 'created_at', 'updated_at', 'deleted_at'];

    public function checkAvatars($data = [])
    {
        foreach ($data as $item) {
            if (!empty($item->avatar) && file_exists('upload/image/' . $item->avatar)) {
                $item->avatar = asset('upload/image/' . $item->avatar);
            } else {
                $item->avatar = asset('upload\image\default-avatar.jpg');
            }
        }
        return $data;
    }

    public function checkAvatar($item)
    {
        if (!empty($item->avatar) && file_exists('upload/image/' . $item->avatar)) {
            $item->avatar = asset('upload/image/' . $item->avatar);
        } else {
            $item->avatar = asset('upload\image\default-avatar.jpg');
        }
        return $item;
    }

    public function list(TableList $request)
    {
        $limit = !empty($request->get('limit')) ? $request->get('limit') : 10;
        $name = $request->get('name');
        $user = $this->getUserData();
        $data = DB::table('users')
            ->where([
                ['id', '<>', $user['id']],
                ['fullname', 'like', '%' . $name . '%']
            ])
            ->orderBy('created_at', 'DESC')
            ->paginate($limit, $this->column);
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $this->checkAvatars($data));
    }

    public function detail(Request $request)
    {
        $code = $request->get('code');
        if ($this->checkExists('users','code', $code)) {
            $data = DB::table('users')
                ->where('code', $code)
                ->first($this->column);
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $this->checkAvatar($data));
        }
        return $this->response->fail('Không tìm thấy người dùng');
    }

    public function create(UserCreate $request)
    {
        $fullname = $request->get('fullname');
        $phone = $request->get('phone');
        $password = Hash::make($request->get('password'));
        $avatar = time();
        $image = $request->file('avatar');
        $image->move('upload/image/', $avatar);
        $email = $request->get('email');
        $address = $request->get('address');
        $introduce_code = $request->get('introduce_code');
        $type = $request->get('type');

        DB::table('users')->insert([
            'code' => $this->generateCode(),
            'fullname' => $fullname,
            'phone' => $phone,
            'password' => $password,
            'avatar' => $avatar,
            'email' => $email,
            'address' => $address,
            'introduce_code' => $introduce_code,
            'type' => $type
        ]);
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }

    public function update(UserUpdate $request)
    {
        $code = $request->get('code');
        $fullname = $request->get('fullname');
        $phone = $request->get('phone');
        $avatar = DB::table('users')->where('code', $code)->value('avatar');
        $email = $request->get('email');
        $address = $request->get('address');
        $introduce_code = $request->get('introduce_code');
        $type = $request->get('type');
        if ($request->file('avatar')) {
            if (file_exists('upload/image/' . $avatar) && $avatar != "") {
                unlink('upload/image/' . $avatar);
            }
            $avatar = time();
            $image = $request->file('avatar');
            $image->move('upload/image/', $avatar);
        }
        DB::table('users')->where('code', $code)->update([
            'fullname' => $fullname,
            'phone' => $phone,
            'avatar' => $avatar,
            'email' => $email,
            'address' => $address,
            'introduce_code' => $introduce_code,
            'type' => $type
        ]);
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }

    public function delete(UserDelete $request)
    {
        $codes = $request->get('codes');
        $codes = array_unique($codes);
        foreach ($codes as $code) {
            DB::table('users')->where('code', $code)->delete();
        }
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }

    public function look(UserDelete $request)
    {
        $codes = $request->get('code');
        $codes = array_unique($codes);
        foreach ($codes as $code) {
            $avatar = DB::table('users')->where('code', $code)->value('avatar');
            if (file_exists('upload/image/' . $avatar) && $avatar != "") {
                unlink('upload/image/' . $avatar);
            }
            DB::table('users')->where('code', $code)->update([
                'deleted_at' => time()
            ]);
        }
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }
}
