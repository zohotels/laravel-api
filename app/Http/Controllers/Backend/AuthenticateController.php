<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use App\Http\Requests\Login;
use App\Models\User;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;

class AuthenticateController extends ControllerBase
{
    public function __construct(ApiResponse $response, Request $request)
    {
        parent::__construct($response, $request);
    }

    /**
     * @param Login $login
     * @return ResponseFactory
     */
    public function login(Login $login)
    {
        $bodyData = $login->post();
        $dataUser = User::userLogin($bodyData, User::TYPE_ADMIN);
        if ($dataUser['status'] === 1) {
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $dataUser['data']);
        }
        return $this->response->fail($dataUser['message']);
    }

    public function logout(){
        
    }
}
