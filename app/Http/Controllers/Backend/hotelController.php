<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\HotelCreate;
use App\Http\Requests\Backend\HotelDelete;
use App\Http\Requests\Backend\HotelUpdate;
use App\Http\Requests\Backend\TableList;
use Illuminate\Http\Request;
use App\Models\BaseModel;
use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Support\Facades\DB;

class hotelController extends ControllerBase
{
    private $column = ['hotel.code', 'hotel.image', 'city.name as city_name', 'hotel.name', 'hotel.address', 'hotel.policy', 'hotel.price', 'hotel.promotion', 'hotel.favorite', 'hotel.created_at', 'hotel.updated_at'];

    public function list(TableList $request)
    {
        $limit = !empty($request->get('limit')) ? $request->get('limit') : 10;
        $name = $request->get('name');
        $data = DB::table('hotel')
            ->leftJoin('city', 'hotel.cityID', 'city.id')
            ->where('hotel.name', 'like', '%' . $name . '%')
            ->orderBy('hotel.id', 'DESC')
            ->paginate($limit, $this->column);
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $this->checkImages($data));
    }

    public function detail(Request $request)
    {
        $code = $request->get('code');
        if ($this->checkExists('hotel','code',$code)) {
            $data = DB::table('hotel')
                ->leftJoin('city', 'hotel.cityID', 'city.id')
                ->where('hotel.code', $code)
                ->first($this->column);
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $this->checkImage($data));
        }
        return $this->response->fail('Mã không tồn tại');
    }

    public function create(HotelCreate $request)
    {
        $cityID = $request->get('cityID');
        $image = time();
        $file = $request->file('image');
        $file->move('upload/image/', $image);
        $name = $request->get('name');
        $address = $request->get('address');
        $policy = $request->get('policy');
        $price = $request->get('price');
        $promotion = $request->get('promotion');
        DB::table('hotel')->insert([
            'code' => $this->generateCode(),
            'cityID' => $cityID,
            'image' => $image,
            'name' => $name,
            'address' => $address,
            'policy' => $policy,
            'price' => "$price",
            'promotion' => "$promotion"
        ]);
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }

    public function update(HotelUpdate $request)
    {
        $code = $request->get('code');
        $cityID = $request->get('cityID');
        $image = $this->getImage('hotel', 'code', $code);
        if ($request->file('image')) {
            if (file_exists('upload/image/' . $image) && $image != "") {
                unlink('upload/image/' . $image);
            }
            $image = time();
            $file = $request->file('image');
            $file->move('upload/image/', $image);
        }
        $name = $request->get('name');
        $address = $request->get('address');
        $policy = $request->get('policy');
        $price = $request->get('price');
        $promotion = $request->get('promotion');
        DB::table('hotel')->where('code', $code)->update([
            'cityID' => $cityID,
            'image' => $image,
            'name' => $name,
            'address' => $address,
            'policy' => $policy,
            'price' => "$price",
            'promotion' => "$promotion"
        ]);
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }

    public function delete(HotelDelete $request){
        $codes = $request->get('code');
        $codes = array_unique($codes);
        foreach ($codes as $code) {
            $image = DB::table('hotel')->where('code', $code)->value('image');
            if (file_exists('upload/image/' . $image) && $image != "") {
                unlink('upload/image/' . $image);
            }
            DB::table('hotel')->where('code', $code)->delete();
        }
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }
}
