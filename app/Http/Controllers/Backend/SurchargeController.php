<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use App\Http\Requests\Backend\SurchargeCreate;
use App\Http\Requests\Backend\SurchargeDelete;
use App\Http\Requests\Backend\SurchargeUpdate;
use App\Http\Requests\Backend\TableList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SurchargeController extends ControllerBase
{
    private $column = ['surcharge.code','hotel.name as hotel_name','surcharge.image','surcharge.name','surcharge.price','surcharge.created_at','surcharge.updated_at'];
    public function list(TableList $request){
        $limit = !empty($request->get('limit')) ? $request->get('limit') : 10;
        $name = $request->get('name');
        $hotel_name = $request->get('hotel_name');
        $data = DB::table('surcharge')
            ->leftJoin('hotel','surcharge.hotelID','hotel.id')
            ->where([
                [
                    'surcharge.name','like','%'.$name.'%'
                ],[
                    'hotel.name', 'like','%'.$hotel_name.'%'
                ]
            ])
            ->orderBy('surcharge.id','DESC')
            ->paginate($limit,$this->column);
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$this->checkImages($data));
    }

    public function detail(Request $request){
        $code = $request->get('code');
        if($this->checkExists('surcharge','code',$code)){
            $data = DB::table('surcharge')
                ->leftJoin('hotel','surcharge.hotelID','hotel.id')
                ->where('surcharge.code',$code)
                ->first($this->column);
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$this->checkImage($data));
        }
        return $this->response->fail('Mã không tồn tại');
    }

    public function create(SurchargeCreate $request){
        $hotelID = $request->get('hotelID');
        $image = time();
        $file = $request->file('image');
        $file->move('upload/image',$image);
        $name = $request->get('name');
        $price = $request->get('price');
        DB::table('surcharge')->insert([
           'code' => $this->generateCode(),
           'hotelID' => $hotelID,
           'image' => $image,
           'name' => $name,
           'price' => $price
        ]);
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }

    public function update(SurchargeUpdate $request){
        $code = $request->get('code');
        $hotelID = $request->get('hotelID');
        $image = DB::table('surcharge')->where('code',$code)->value('image');
        if($request->file('image')){
            if (file_exists('upload/image/' . $image) && $image != "") {
                unlink('upload/image/' . $image);
            }
            $image = time();
            $file = $request->file('image');
            $file->move('upload/image/', $image);
        }
        $name = $request->get('name');
        $price = $request->get('price');
        DB::table('surcharge')->where('code',$code)->update([
            'hotelID' => $hotelID,
            'image' => $image,
            'name' => $name,
            'price' => $price
        ]);
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }

    public function delete(SurchargeDelete $request){
        $codes = $request->get('code');
        $codes = array_unique($codes);
        foreach ($codes as $code) {
            $image = DB::table('surcharge')->where('code', $code)->value('image');
            if (file_exists('upload/image/' . $image) && $image != "") {
                unlink('upload/image/' . $image);
            }
            DB::table('surcharge')->where('code', $code)->delete();
        }
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }
}
