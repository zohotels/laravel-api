<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use App\Http\Requests\Backend\ImageCreate;
use App\Http\Requests\Backend\ImageDelete;
use App\Http\Requests\Backend\TableList;
use App\Models\BaseModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ImageController extends ControllerBase
{
    private $column = ['id','image'];

    public function list(TableList $request){
        $limit = !empty($request->get('limit')) ? $request->get('limit') : 10;
        $hotelID = $request->get('hotelID');
        if($this->checkExists('hotel','id', $hotelID)){
            $data = DB::table('image')
                ->where('hotelID',$hotelID)
                ->orderBy('id','DESC')
                ->paginate($limit,$this->column);
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$this->checkImages($data));
        }
        return $this->response->fail('ID không tồn tại');
    }

    public function create(ImageCreate $request){
        $hotelID = $request->get('hotelID');
        $files = $request->file('image');
        $files = array_unique($files);
        foreach($files as $file){
            $image = time().$this->generateCode();
            $file->move('upload/image/',$image);
            DB::table('image')->insert([
               'hotelID' => $hotelID,
               'image' => $image
            ]);
        }
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }

    public function delete(ImageDelete $request){
        $ids = $request->get('id');
        $ids = array_unique($ids);
        foreach ($ids as $id) {
            $image = DB::table('image')->where('id', $id)->value('image');
            if (file_exists('upload/image/' . $image) && $image != "") {
                unlink('upload/image/' . $image);
            }
            DB::table('image')->where('id', $id)->delete();
        }
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
    }
}
