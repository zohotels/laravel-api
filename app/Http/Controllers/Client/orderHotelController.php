<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use DB;
use Validator;

class orderHotelController extends ControllerBase
{
    public function index(){
        $user = $this->getUserData();
        $data = DB::table('order_hotel')
        ->select('hotel_name','hotel_address','hotel_room_number','hotel_guest_number','hotel_day_number','status','aspect')
        ->leftJoin('hotel','order_hotel.hotel_id','=','hotel.hotel_id')
        ->where('user_id',$user['id'])
        ->get();
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$data);
    }

    public function show($id){
        if(DB::table('order_hotel')->where('id',$id)->exists()){
            $user = $this->getUserData();
            $data = DB::table('order_hotel')
            ->select('hotel_name','hotel_address','hotel_room_number','hotel_guest_number','hotel_day_number','status','aspect','time_receive','time_return','receive_early','return_late','preferential_id','email','reservation_for','fullname','phone','total','payment')
            ->leftJoin('hotel','order_hotel.hotel_id','=','hotel.hotel_id')
            ->where([['user_id',$user['id']],['id',$id]])
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$data);
        }
        return $this->response->fail('ID không tồn tại');
    }

    public function create(Request $request){
        $validation = Validator::make($request->all(), [
            'hotel_id'          => 'required|exists:hotel,hotel_id',
            'time_receive'      => 'required|string',
            'time_return'       => 'required|string',
            'receive_early'     => 'in:1,0',
            'return_late'       => 'in:1,0',
            'preferential_id'   => 'nullable|exists:preferential,preferential_id',
            'email'             => 'required|email|',
            'reservation_for'   => 'required|in:1,0',
            'fullname'          => 'required|string|min:0|max:255',
            'phone'             => 'required|numeric',
            'notes'             => 'nullable|string',
            'total'             => 'required|numeric',
            'payment'           => 'required|in:0,1'
        ], [
            'hotel_id.required'         => 'ID khách sạn không được để trống',
            'hotel_id.exists'           => 'ID khách sạn không được để trống',
            'time_receive.required'     => 'Thời gian nhận phòng không được đẻ trống',
            'time_receive.string'       => 'Thời gian nhận phòng không phải là chuỗi',
            'time_return.required'      => 'Thời gian trả phòng không được để trống',
            'time_return.string'        => 'Thời gian trả phòng không phải là chuỗi',
            'receive_early.in'          => 'Nhận phòng sớm là một trong các giá trị [:values]',
            'return_late.in'            => 'Trả phòng là một trong các giá trị [:values]',
            'preferential_id.exists'    => 'ID mã giảm giá không tồn tại',
            'email.required'            => 'Email không được để trống',
            'email.email'               => 'Email không hợp lệ',
            'reservation_for.reuqired'  => 'Đối tượng đặt phòng không được để trống',
            'reservation_for.in'        => 'Đối tượng đặt phòng là một trong các giá trị [:values]',
            'fullname.required'         => 'Tên khách hàng không được để trống',
            'fullname.string'           => 'Tên khách hàng không phải là chuỗi',
            'fullname.min'              => 'Tên khách hàng phải lớn hơn :min kí tự',
            'fullname.max'              => 'Tên khách hàng không được vượt qua :max kí tự',
            'phone.required'            => 'Số điện thoại không được để trống',
            'phone.numeric'             => 'Số điện thoại không phải là số',
            'notes.string'              => 'Ghi chú không phải là chuỗi',
            'total.required'            => 'Tổng tiền không được để trống',
            'total.numeric'             => 'Tổng tiền không phải là số',
            'payment.required'          => 'Hình thức thanh toán không được để trống',
            'payment.in'                => 'Hình thức thanh toán là các giá trị [:o,1]'
        ]);
        if($validation->passes()){
            $user_id            = ($this->getUserData())['id'];
            $hotel_id           = $request->get('hotel_id');
            $time_receive       = $request->get('time_receive');
            $time_return        = $request->get('time_return');
            $receive_early      = $request->get('receive_early');
            $return_late        = $request->get('return_late');
            $preferential_id    = $request->get('preferential_id');
            $reservation_for    = $request->get('reservation_for');
            $email              = $request->get('email');
            $fullname           = $request->get('fullname');
            $phone              = $request->get('phone');
            $notes              = $request->get('notes');
            $total              = $request->get('total');
            $payment            = $request->get('payment');
            $data = DB::table('order_hotel')->insertGetId([
                'user_id'           => $user_id,
                'hotel_id'          => $hotel_id,
                'time_receive'      => $time_receive,
                'time_return'       => $time_return,
                'receive_early'     => $receive_early,
                'return_late'       => $return_late,
                'preferential_id'   => $preferential_id,
                'reservation_for'   => $reservation_for,
                'email'             => $email,
                'fullname'          => $fullname,
                'phone'             => $phone,
                'notes'             => $notes,
                'total'             => $total
            ]);
            return $this->response->success('Đặt phòng thành công',$data);
        }
        return $this->response->fail($validation->errors()->all());
    }

    public function room_create(Request $request){
        $validation = Validator::make($request->all(),[
            'order_hotel_id'    => 'required|exists:order_hotel,id',
            'room_id'           => 'required|exists:room,room_id',
            'order_room_price'  => 'required|numeric'
        ],[
            'order_hotel_id.required'   => 'ID đơn phòng không được để trống',
            'order_id.exists'           => 'ID đơn hàng không tồn tại',
            'room_id.required'          => 'ID phòng khách sạn không được để trống',
            'room_id.numeric'           => 'ID phòng phòng khách sạn không phải là số',
            'order_room_price.required' => 'Giá phòng không được để trống',
            'order_room_price.numeric'  => 'Giá phòng không phải là số'   
        ]);
        if($validation->passes()){
            $order_hotel_id = $request->get('order_hotel_id');
            $room_id        = $request->get('room_id');
            $order_rom_pice = $request->get('order_room_price');
            if(DB::table('order_room')->where([['order_hotel_id',$order_hotel_id],['room_id',$room_id]])->doesntExist()){
                DB::table('order_room')->insert([
                    'order_hotel_id'    => $order_hotel_id,
                    'room_id'           => $room_id,
                    'order_room_price'  => $order_rom_pice
                ]);
                return $this->response->success('Thêm phòng thành công');
            }
            else{
                DB::table('order_room')->where([
                    'order_hotel_id'   => $order_hotel_id,
                    'room_id'          => $room_id
                ])->delete();
                return $this->response->success('Xóa phòng thành công');
            }
        }
        return $this->response->fail($validation->errors()->all());
    }

    public function status(Request $request,$id){
        $validation = Validator::make($request->all(), [
            'value'    => 'required|numeric|in:0,1,2,3'
        ], [
            'value.required'    => 'Giá trị không được để trống',
            'value.numeric'     => 'Giá trị không phải là số',
            'value.in'          => 'Giá trị phải là một trong những giá trị [:values]',
        ]);
        if($validation->passes()){
            $value = $request->get('value');
            if(DB::table('order_hotel')->where('id',$id)->exists()){
                DB::table('order_hotel')
                ->where('id',$id)
                ->update(['status'=>$value]);
                return $this->response->success('Cập nhật thành công');
            }
            return $this->response->fail('ID không tồn tại');
        }
        return $this->response->fail($validation->errors()->all());
    }

    public function aspect(Request $request,$id){
        $validation = Validator::make($request->all(), [
            'value'    => 'required|numeric|in:0,1'
        ], [
            'value.required'   => 'Giá trị không được để trống',
            'value.numeric'    => 'Giá trị không phải là số',
            'value.in'         => 'Giá trị phải là một trong những giá trị [:values]',
        ]);
        if($validation->passes()){
            $value = $request->get('value');
            if(DB::table('order_hotel')->where('id',$id)->exists()){
                DB::table('order_hotel')
                ->where('id',$id)
                ->update(['aspect'=>$value]);
                return $this->response->success('Cập nhật thành công');
            }
            return $this->response->fail('ID không tồn tại');
        }
        return $this->response->fail($validation->errors()->all());
    }

    public function room_index($id){
        if(DB::table('order_hotel')->where('id',$id)->exists()){
            $data = DB::table('order_room')
            ->select('id','room_name','order_room_price')
            ->leftJoin('room','order_room.room_id','=','room.room_id')
            ->where('order_hotel_id',$id)
            ->orderBy('order_room_created','DESC')
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$data);
        }
        return $this->response->fail('ID không tồn tại');
    }

    public function surcharge_index($id){
        if(DB::table('order_hotel')->where('id',$id)->exists()){
            $data = DB::table('order_surcharge')
            ->select('id','surcharge_name','order_surcharge_price')
            ->leftJoin('surcharge','order_surcharge.surcharge_id','=','surcharge.surcharge_id')
            ->where('order_hotel_id',$id)
            ->orderBy('order_surcharge_created','DESC')
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$data);
        }
        return $this->response->fail('ID không tồn tại');
    }

    public function surcharge_create(Request $request){
        $validation = Validator::make($request->all(),[
            'order_hotel_id'        => 'required|exists:order_hotel,id',
            'surcharge_id'          => 'required|exists:surcharge,surcharge_id',
            'order_surcharge_price' => 'required|numeric'
        ],[
            'order_hotel_id.required'       => 'ID đơn phòng không được để trống',
            'order_hotel_id.exists'         => 'ID đơn phòng không tồn tại',
            'surcharge_id.required'         => 'ID phụ phí không được để trống',
            'surcharge_id.exists'           => 'ID phụ phí không tồn tại',
            'order_surcharge_price.required'=> 'Giá phụ phí không được để trống',
            'order_surcharge_price.numeric' => 'Giá phụ phí không phải là số'
        ]);
        if($validation->passes()){
            $order_hotel_id         = $request->get('order_hotel_id');
            $surcharge_id           = $request->get('surcharge_id');
            $order_surcharge_price  = $request->get('order_surcharge_price');
            if(DB::table('order_surcharge')->where([['order_hotel_id',$order_hotel_id],['surcharge_id',$surcharge_id]])->doesntExist()){
                DB::table('order_surcharge')->insert([
                    'order_hotel_id'        => $order_hotel_id,
                    'surcharge_id'          => $surcharge_id,
                    'order_surcharge_price' => $order_surcharge_price
                ]);
                return $this->response->success('Thêm phụ phí thành công');
            }else{
                DB::table('order_surcharge')->where([
                    'order_hotel_id'        => $order_hotel_id,
                    'surcharge_id'          => $surcharge_id
                ])->delete();
                return $this->response->success('Xóa phụ phí thành công');
            }
            
        }
        return $this->response->fail($validation->errors()->all());
    }
}
