<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use DB;

class hotelController extends ControllerBase
{
    public function show($id)
    {
        if(DB::table('hotel')->where('hotel_id',$id)->exists()){
            $data = DB::table('hotel')
            ->select('hotel_id','hotel_city_id','hotel_image','hotel_address','hotel_room_number','hotel_guest_number','hotel_day_number','hotel_price','hotel_promotion')
            ->where([['hotel_id',$id],['hotel_status',1]])
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        return $this->response->fail('ID không tồn tại');
    }

    public function room($id){
        if(DB::table('hotel')->where('hotel_id',$id)->exists()){
            $data = DB::table('room')
            ->select('room_id','room_hotel_id','room_image','room_name','room_bed_type','room_price','room_promotion')
            ->where('room_hotel_id',$id)
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        return $this->response->fail('ID không tồn tại');
    }

    public function image($id){
        if(DB::table('hotel')->where('hotel_id',$id)->exists()){
            $data = DB::table('image')
            ->select('image_id','image_hotel_id','image_name')
            ->where('image_hotel_id',$id)
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        return $this->response->fail('ID không tồn tại');
    }
}
