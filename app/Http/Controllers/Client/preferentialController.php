<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Http\Request;
use DB;

class preferentialController extends ControllerBase
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $data = DB::table('preferential')
            ->select('preferential_id','preferential_name','preferential_image')
            ->where('preferential_status',1)
            ->orderBy('preferential_created','DESC')
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$data);
        }
        catch(Exception $exception){
            return $this->response->fail(ApiResponse::MESSAGE_FAILED, ['message'=>'ID không tồn tại']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(DB::table('preferential')->where('preferential_id',$id)->exists()){
            $data = DB::table('preferential')
            ->select('preferential_id','preferential_name','preferential_image','preferential_condition','preferential_discount_code')
            ->where([['preferential_status',1],['preferential_id',$id]])
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$data);
        }
        return $this->response->fail('ID không tồn tại');
    }
}
