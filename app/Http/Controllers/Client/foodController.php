<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use DB;

class foodController extends ControllerBase
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $data = DB::table('food')
            ->where([['food_id',$id],['food_status',1]])
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        catch(Exception $exception){
            return $this->response->fail(ApiResponse::MESSAGE_FAILED, ['message'=>'ID không tồn tại']);
        }
    }
}
