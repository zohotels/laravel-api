<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use DB;

class cityController extends ControllerBase
{
    public function index()
    {
        $data = DB::table('city')
        ->select('city_id','city_image','city_name')
        ->where('city_status',1)
        ->orderBy('city_created','DESC')
        ->get();
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
    }

    public function hightlight()
    {
        $data = DB::table('city')
        ->select('city_id','city_image','city_name')
        ->where([['city_status',1],['city_hightlight',1]])
        ->get();
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
    }

    public function show($id)
    {
        if(DB::table('city')->where('city_id',$id)->exists()){
            $data = DB::table('city')
            ->select('city_id','city_image','city_name')
            ->where([['city_status',1],['city_id',$id]])
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        return $this->response->fail('ID không tồn tại');
    }

    public function hotel($id)
    {
        if(DB::table('city')->where('city_id',$id)->exists()){
            $data = DB::table('hotel')
            ->select('hotel_id','hotel_city_id','hotel_image','hotel_address','hotel_room_number','hotel_guest_number','hotel_day_number','hotel_price','hotel_promotion')
            ->where([['hotel_status',1],['hotel_city_id',$id]])
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        return $this->response->fail('ID không tồn tại');
    }
}
