<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use DB;
use Validator;

class favoriteController extends ControllerBase
{
    public function hotel($id){
        if(DB::table('hotel')->where('hotel_id',$id)->exists()){
            $data = DB::table('favorite')
            ->where('favorite_hotel_id',$id)
            ->count();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, ['message'=>$data]);
        }
        return $this->response->fail('ID không tồn tại');
    }

    /**
     * Lấy danh sách khách sạn người dùng yêu thích.
     * @
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $userData = $this->getUserData();
        $data = DB::table('favorite')
        ->select('hotel_id','hotel_image','hotel_name','hotel_address')
        ->leftJoin('hotel','favorite.favorite_hotel_id','hotel.hotel_id')
        ->where('favorite_user_id',$userData['id'])
        ->get();
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
    }

    public function create(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'hotel_id'    => 'required|exists:hotel,hotel_id|numeric',
        ],[
            'hotel_id.required'   => 'ID khách sạn không được để trống',
            'hotel_id.exists'     => 'ID khách sạn không tồn tại',
            'hotel_id.numeric'    => 'ID khách sạn không phải là số'
        ]);
        if($validation->passes()){
            $userData       = $this->getUserData();
            $user_id        = $userData['id'];
            $hotel_id       = $request->get('hotel_id');
            if(DB::table('favorite')->where([['favorite_user_id',$user_id],['favorite_hotel_id' , $hotel_id]])->doesntExist()){
                DB::table('favorite')->insert([
                    'favorite_user_id'      => $user_id,
                    'favorite_hotel_id'     => $hotel_id,
                ]);
                return $this->response->success('Đã thêm yêu thích');
            }
            else{
                DB::table('favorite')
                ->where([['favorite_user_id',$user_id],['favorite_hotel_id' , $hotel_id]])
                ->delete();
                return $this->response->success('Đã hủy yêu thích');
            }
            
        }
        return $this->response->fail($validation->errors()->all());
    }

    public function delete($id){
        if(DB::table('favorite')->where([['favorite_user_id',$user_id],['favorite_hotel_id' , $id]])->exists()){
            $userData       = $this->getUserData();
            $user_id        = $userData['id'];
            $hotel_id       = $id;
            DB::table('favorite')
            ->where([['favorite_user_id',$user_id],['favorite_hotel_id' , $hotel_id]])
            ->delete();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
        }
        return $this->response->fail('ID không tồn tại');
    }

    public function check($id){
        $userData = $this->getUserData();
        if(DB::table('favorite')->where([['favorite_user_id',$userData['id']],['favorite_hotel_id' , $id]])->exists()){
            $data = DB::table('favorite')
            ->where([['favorite_user_id',$userData['id']],['favorite_hotel_id',$id]])
            ->get();
            return $this->response->success('true');
        }
        return $this->response->fail('ID không tồn tại');
    }
}
