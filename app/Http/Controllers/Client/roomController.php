<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use DB;

class roomController extends ControllerBase
{
    /**
     * Lấy thông tin phòng
     * @param int $id
     * @return response
     */
    public function show($id){
        try{
            $data = DB::table('room')
            ->where('room_id',$id)
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        catch(Exception $exception){
            return $this->response->fail(ApiResponse::MESSAGE_FAILED, ['message'=>'ID không tồn tại']);
        }
    }

    
}
