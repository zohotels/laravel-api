<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use DB;

class evaluateController extends ControllerBase
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $data = DB::table('evaluate')->orderBy('evaluate_created','DESC')->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        catch(Exception $exception){
            return $this->response->fail(ApiResponse::MESSAGE_FAILED, ['message'=>'ID không tồn tại']);
        }
    }
    
    /**
     * Display a user of the resource.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function user($id)
    {
        try{
            $data = DB::table('evaluate')->where('evaluate_user_id',$id)->orderBy('evaluate_created','DESC')->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        catch(Exception $exception){
            return $this->response->fail(ApiResponse::MESSAGE_FAILED, ['message'=>'ID không tồn tại']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'evaluation_user_id'    => 'required|exists:evaluation,evaluation_user_id',
            'evaluation_star'       => 'required|numeric|min:0|max:5',
            'evaluation_content'    => 'required|string'
        ],[
            'evaluation_user_id.required'   => 'ID người dùng không được để trống',
            'evaluation_user_id.exists'     => 'ID người dùng không tồn tại',
            'evaluation_star.required'      => 'Điểm đánh giá không được để trống',
            'evaluation_star.numeric'       => 'Điểm đánh giá không phải là số',
            'evaluation_star.min'           => 'Điểm đánh giá không được nhỏ hơn :min',
            'evaluation_star.max'           => 'Điểm đánh giá không được lớn hơn :max',
            'evaluation_content.required'   => 'Nội dung đánh giá không được để trống',
            'evaluation_content.string'     => 'Nội dung đánh giá không phải là chuỗi'
        ]);
        if($validation->passes()){
            $evaluation_user_id     = $request->get('evaluation_user_id');
            $evaluation_star        = $request->get('evaluation_star');
            $evaluation_content     = $request->get('evaluation_content');
            try{
                DB::table('evaluation')->insert([
                    'evaluation_user_id'    => $evaluation_user_id,
                    'evaluation_star'       => $evaluation_star,
                    'evaluation_content'    => $evaluation_content
                ]);
                return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
            }
            catch (Exception $exception) {
                return $this->response->fail(ApiResponse::MESSAGE_FAILED, ['message'=>'Đánh giá thất bại']);
            }
        }
        return $this->response->fail(ApiResponse::MESSAGE_FAILED,['message'=>$validation->errors()->all()]);
    }
}
