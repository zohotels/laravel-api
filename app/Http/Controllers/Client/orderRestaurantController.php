<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use DB;
use Validator;

class orderRestaurantController extends ControllerBase
{
    public function index(){
        $data = DB::table('order_restaurant')
        ->select('id','restaurant_name','restaurant_address','total','status','aspect','order_restaurant_created','order_restaurant_updated')
        ->leftJoin('restaurant','order_restaurant.restaurant_id','=','restaurant.restaurant_id')
        ->where('user_id',($this->getUserData())['id'])
        ->orderBy('order_restaurant_created','DESC')
        ->get();
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$data);
    }

    public function show($id){
        if(DB::table('order_restaurant')->where('id',$id)->exists()){
            $user = $this->getUserData();
            $data = DB::table('order_restaurant')
            ->select('id','restaurant_name','restaurant_address','total','status','aspect','order_restaurant_created','order_restaurant_updated')
            ->leftJoin('restaurant','order_restaurant.restaurant_id','=','restaurant.restaurant_id')
            ->where([['user_id',$user['id']],['id',$id]])
            ->orderBy('order_restaurant_created','DESC')
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$data);
        }
        return $this->response->fail('ID không tồn tại');
    }

    public function create(Request $request){
        $validation = Validator::make($request->all(),[
            'restaurant_id'     => 'required|exists:restaurant,restaurant_id',
            'total'             => 'required|numeric',
        ],[
            'restaurant_id.required'    => 'ID cửa hàng không được để trống',
            'restaurant_id.exists'      => 'ID cửa hàng không tồn tại',
            'total.required'            => 'Tổng tiền không được để trống',
            'total.numeric'             => 'Tổng tiền không phải là số'
        ]);
        if($validation->passes()){
            $user           = $this->getUserData();
            $restaurant_id  = $request->get('restaurant_id');
            $total          = $request->get('total');
            $data = DB::table('order_restaurant')
            ->insertGetId([
                'user_id'       => $user['id'],
                'restaurant_id' => $restaurant_id,
                'total'         => $total
            ]);
            return $this->response->success('Thêm đơn hàng thành công',$request->all());
        }
        return $this->response->fail(ApiResponse::MESSAGE_FAILED,$validation->errors()->all());
    }

    public function food_index($id){
        if(DB::table('order_restaurant')->where('id',$id)->exists()){
            $user = $this->getUserData();
            $data = DB::table('order_food')
            ->select('id','food_name','amount','food_price')
            ->leftJoin('food','order_food.food_id','=','food.food_id')
            ->where('order_restaurant_id',$id)
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$data);
        }
        return $this->response->fail('ID không tồn tại');
    }

    public function food_create(Request $request){
        $validation = Validator::make($request->all(),[
            'order_restaurant_id'       => 'required|exists:order_restaurant,id',
            'food_id'                   => 'required|exists:food,food_id',
            'amount'                    => 'required|numeric|min:1',
            'order_food_price'          => 'required|numeric'
        ],[
            'order_restaurant_id.required'      => 'ID đơn hàng không được để trống',
            'order_restaurant_id.exists'        => 'ID đơn hàng không tồn tại',
            'food_id.required'                  => 'ID món ăn không được để trống',
            'food_id.exists'                    => 'ID món ăn không được để trống',
            'amount.required'                   => 'Số lượng không được để trống',
            'amount.numeric'                    => 'Số lượng không phải là số',
            'amount.min'                        => 'Số lượng phải lớn hơn hoặc bằng :min',
            'order_food_price.required'         => 'Giá món ăn không được để trống',
            'order_food_price.numeric'          => 'Giá món ăn không phải là số'
        ]);
        if($validation->passes()){
            $order_restaurant_id        = $request->get('order_restaurant_id');
            $food_id                    = $request->get('food_id');
            $amount                     = $request->get('amount');
            $order_food_price           = $request->get('order_food_price'); 
            DB::table('order_food')
            ->insert([
                'order_restaurant_id'   => $order_restaurant_id,
                'food_id'               => $food_id,
                'amount'                => $amount,
                'order_food_price'      => $order_food_price
            ]);
            return $this->response->success('Thêm món ăn thành công');
        }
        return $this->response->fail($validation->errors()->all());
    }
}
