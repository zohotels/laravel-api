<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use DB;
use Validator;

class payinController extends ControllerBase
{
    public function index(){
        $userData = $this->getUserData();
        try{
            $data = DB::table('payin')
            ->select('payin_id','payin_recharge_from','payin_value','payin_status')
            ->where('payin_user_id',$userData['id'])
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        catch(Exception $exception){
            return $this->response->fail(ApiResponse::MESSAGE_FAILED, ['message'=>'ID không tồn tại']);
        }
    }

    public function create(Request $request){
        $validation = Validator::make($request->all(), [
            'recharge_from'     => 'required|string|max:255',
            'value'             => 'required|numeric|min:1'
        ], [
            'recharge_from.required'    => 'Tên nơi nạp tiền không được để trống',
            'recharge_from.string'      => 'Tên nơi nạp tiền không phải là chuỗi',
            'recharge_from.max'         => 'Tên nơi nạp tiền không được vượt qua :max kí tự',
            'value.required'            => 'Số tiền nạp vào không được để trống',
            'value.numeric'             => 'Số tiền nạp vào không phải là số',
            'value.min'                 => 'Số tiền nạp vào phải lớn hơn :min'
        ]);
        if($validation->passes()){
            $userData = $this->getUserData();
            $recharge_from  = $request->get('recharge_from');
            $value          = $request->get('value');
            DB::table('payin')->insert([
                'payin_user_id'         => $userData['id'],
                'payin_recharge_from'   => $recharge_from,
                'payin_value'           => $value
            ]);
            $valueAcc   = DB::table('account')
            ->where('account_user_id',$userData['id'])
            ->value('account_value');
            $account_value = $valueAcc+$value;
            DB::table('account')
            ->where('account_user_id',$userData['id'])
            ->update(['account_value' => $account_value]);
            return $this->response->success('Nạp tiền thành công');
        }
        return $this->response->fail(ApiResponse::MESSAGE_FAILED,['message'=>$validation->errors()->all()]);
    }
}
