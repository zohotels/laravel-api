<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use DB;
use Validator;

class feedbackController extends ControllerBase
{
    public function hotelCount($id){
        if(DB::table('hotel')->where('hotel_id',$id)->exists()){
            $data = DB::table('feedback')
            ->where('feedback_hotel_id',$id)
            ->count();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, ['count' => $data]);
        }
        return $this->response->fail('ID không tồn tại');
    }

    public function hotel($id){
        if(DB::table('hotel')->where('hotel_id',$id)->exists()){
            $data = DB::table('feedback')
            ->select('feedback_id','image','fullname','feedback_content','feedback_created')
            ->leftJoin('users','feedback.feedback_user_id','users.id')
            ->where('feedback_hotel_id',$id)
            ->orderBy('feedback_created','DESC')
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        return $this->response->fail('ID không tồn tại');
    }

    public function create(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'hotel_id'   => 'required|exists:hotel,hotel_id|numeric',
            'star'       => 'required|numeric|min:0|max:5',
            'content'    => 'required|string'
        ],[
            'hotel_id.required'  => 'ID khách sạn không được để trống',
            'hotel_id.exists'    => 'ID khách sạn không tồn tại',
            'hotel_id.numeric'   => 'ID khách sạn không phải là số',
            'star.required'      => 'Điểm đánh giá không được để trống',
            'star.numeric'       => 'Điểm đánh giá không phải là số',
            'star.min'           => 'Điểm đánh giá không được nhỏ hơn :min',
            'star.max'           => 'Điểm đánh giá không được lớn hơn :max',
            'content.required'   => 'Nội dung đánh giá không được để trống',
            'content.string'     => 'Nội dung đánh giá không phải là chuỗi'
        ]);
        if($validation->passes()){
            $userData             = $this->getUserData();
            $feedback_user_id     = $userData['id'];
            $feedback_hotel_id    = $request->get('hotel_id');
            $feedback_star        = $request->get('star');
            $feedback_content     = $request->get('content');
            DB::table('feedback')->insert([
                'feedback_user_id'      => $feedback_user_id,
                'feedback_hotel_id'     => $feedback_hotel_id,
                'feedback_star'         => $feedback_star,
                'feedback_content'      => $feedback_content
            ]);
            return $this->response->success('Thêm thành công');
        }
        return $this->response->fail($validation->errors()->all());
    }

    public function update(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'id'         => 'required|exists:feedback,feedback_id',
            'star'       => 'required|numeric|min:0|max:5',
            'content'    => 'required|string'
        ],[
            'id.required'        => 'ID đánh giá không được để trống',
            'id.exists'          => 'ID đánh giá không tồn tại',
            'star.required'      => 'Điểm đánh giá không được để trống',
            'star.numeric'       => 'Điểm đánh giá không phải là số',
            'star.min'           => 'Điểm đánh giá không được nhỏ hơn :min',
            'star.max'           => 'Điểm đánh giá không được lớn hơn :max',
            'content.required'   => 'Nội dung đánh giá không được để trống',
            'content.string'     => 'Nội dung đánh giá không phải là chuỗi'
        ]);
        if($validation->passes()){
            $userData             = $this->getUserData();
            $feedback_id          = $request->get('id');
            $feedback_star        = $request->get('star');
            $feedback_content     = $request->get('content');
            DB::table('feedback')->where('feedback_id',$feedback_id)->update([
                'feedback_star'         => $feedback_star,
                'feedback_content'      => $feedback_content
            ]);
            return $this->response->success('Cập nhật thành công');
        }
        return $this->response->fail($validation->errors()->all());
    }

    public function delete($id){
        if(DB::table('feedback')->where('feedback_id',$id)->exists()){
            DB::table('feedback')->where('feedback_id',$id)->delete();
            return $this->response->success('Xóa thành công');
        }
        return $this->response->fail('ID không tồn tại');
    }

}
