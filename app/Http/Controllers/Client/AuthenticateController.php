<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use App\Http\Requests\Login;
use App\Models\User;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use DB;
use Validator;
use Hash;

class AuthenticateController extends ControllerBase
{
    public function __construct(ApiResponse $response, Request $request)
    {
        parent::__construct($response, $request);
    }

    /**
     * @param Login $login
     * @return ResponseFactory
     */
    public function login(Login $login)
    {
        $bodyData = $login->post();
        $dataUser = User::userLogin($bodyData, User::TYPE_CLIENT);
        if ($dataUser['status'] === 1) {
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $dataUser['data']);
        }
        return $this->response->fail($dataUser['message']);
    }

    public function register(Request $request){
        $validation = Validator::make($request->all(),[
            'fullname'      => 'required|string|min:1|max:255',
            'phone'         => 'required|numeric|unique:users,phone',
            'password'      => 'required|string|min:6|max:255|regex:/^\S+$/',
            'password_re'   => 'required|string|min:6|max:255|regex:/^\S+$/|same:password',
            'introduce_code'=> 'exists:users,phone',
        ],[
            'fullname.required'         => 'Họ và tên không được để trống',
            'fullname.string'           => 'Họ và trên không phải là chuỗi',
            'fullname.min'              => 'Họ và tên phải có ít nhất :min kí tự',
            'fullname.max'              => 'Họ và tên không được vượt quá :max kí tự',
            'phone.required'            => 'Số điện thoại không được để trống',
            'phone.numeric'             => 'Số điện thoại không phải là số',
            'phone.unique'              => 'Số điện thoại đã tồn tại',
            'password.required'         => 'Mật khẩu không được để trống',
            'password.string'           => 'Mật khẩu không phải là chuỗi',
            'password.min'              => 'Mật khẩu phải có ít nhất :min kí tự',
            'password.max'              => 'Mật khẩu không đượt vượt quá :max kí tự',
            'password.regex'            => 'Mật khẩu không hợp lệ',
            'password_re.required'      => 'Mật khẩu nhập lại không được để trống',
            'password_re.string'        => 'Mật khẩu nhập lại không phải là chuỗi',
            'password_re.min'           => 'Mật khẩu nhập lại phải có ít nhất :min kí tự',
            'password_re.max'           => 'Mật khẩu nhập lại không được vượt quá :max kí tự',
            'password_re.regex'         => 'Mật khẩu nhập lại không hợp lệ',
            'password_re.same'          => 'Mật khẩu nhập lại không khớp',
            'introduce_code.exists'     => 'Mã giới thiệu không tồn tại'
        ]);
        if($validation->passes()){
            $fullname           = $request->get('fullname');
            $phone              = $request->get('phone');
            $password           = $request->get('password');
            $password           = Hash::make($password);
            $introduce_code     = $request->get('introduce_code');
            try{
                DB::table('users')->insert([
                    'fullname'      => $fullname,
                    'phone'         => $phone,
                    'password'      => $password,
                    'introduce_code'=> $introduce_code,
                    'type'          => 'client'
                ]);
                return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$request->all());
            }
            catch(Exception $exception){
                return $this->response->fail(ApiResponse::MESSAGE_FAILED);
            }
        }
        return $this->response->fail(ApiResponse::MESSAGE_FAILED,$validation->errors()->all());
    }

    public function show(){
        try{
            $user = $this->getUserData();
            $data = DB::table('users')
            ->select('image','fullname','address','phone','email')
            ->where('id',$user['id'])
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$data);
        }
        catch(Exception $exception){
            return $this->response->fail(ApiResponse::MESSAGE_FAILED);
        }
    }

    public function update(Request $request){
        $validation = Validator::make($request->all(),[
            'image'         => 'image|mimes:jpeg,png,jpg,gif|dimensions:ratio=1/1|max:5000',
            'fullname'      => 'string|min:1|max:255',
            'address'       => 'string|min:1|max:255',
            'phone'         => 'numeric|unique:users,phone',
            'email'         => 'email|min:5|max:255',
        ],[
            'image.image'       => 'Ảnh tải lên không phải là ảnh',
            'image.mimes'       => 'Ảnh tải lên phải có định dạng :values ',
            'image.dimensions'  => 'Ảnh phải có tỉ lệ 1/1',
            'image.max'         => 'Ảnh không được vượt quá :max kB',
            'fullname.string'   => 'Họ và tên không phải là chuỗi',
            'fullname.min'      => 'Họ và tên phải có ít nhất :min kí tự',
            'fullname.max'      => 'Họ và tên không được vượt quá :max kí tự',
            'address.string'    => 'Địa chỉ không phải là chuỗi',
            'address.min'       => 'Địa chỉ phải có ít nhất :min kí tự',
            'address.max'       => 'Địa chỉ không được vượt quá :max kí tự',
            'phone.numeric'     => 'Số điện thoại không phải là số',
            'phone.unique'      => 'Số điện thoại đã tồn tại',
            'email.email'       => 'Email không đúng định dạng',
            'email.min'         => 'Email phải có ít nhất :min kí tự',
            'email.max'         => 'Email không được vượt quá :max kí tự' 
        ]);
        if($validation->passes()){
            $user           = $this->getUserData();
            $image          = time();$request->file('image')->move('upload/image/',$image);
            $fullname       = $request->get('fullname');
            $address        = $request->get('address');
            $phone          = $request->get('phone');
            $email          = $request->get('email');
            try{
                DB::table('users')
                ->where('id',$user['id'])
                ->update([
                    'image'     => $image,
                    'fullname'  => $fullname,
                    'address'   => $address,
                    'phone'     => $phone,
                    'email'     => $email
                ]);
                return $this->response->success(ApiResponse::MESSAGE_SUCCESS);
            }
            catch(Exception $exception){
                return $this->response->fail(ApiResponse::MESSAGE_FAILED);
            }
        }
        return $this->response->fail(ApiResponse::MESSAGE_FAILED,$validation->errors()->all());
    }

    public function change_password(Request $request){
        $validation = Validator::make($request->all(),[
            'password_old'  => 'required|string|max:255|min:6|regex:/^\S+$/',
            'password_new'  => 'required|string|max:255|min:6|regex:/^\S+$/',
            'password_res'  => 'required|string|max:255|min:6|regex:/^\S+$/|same:password_new',
        ],[
            'password_old.required'     => 'Mật khẩu cũ không được để trống',
            'password_old.string'       => 'Mật khẩu cũ không phải là chuỗi',
            'password_old.min'          => 'Mật khẩu cũ phải có ít nhất :min kí tự',
            'password_old.max'          => 'Mật khẩu cũ không được vượt quá :max kí tự',
            'password_old.regex'        => 'Mật khẩu cũ không hợp lệ',
            'password_new.required'     => 'Mật khẩu mới không được để trống',
            'password_new.string'       => 'Mật khẩu mới không phải là chuỗi',
            'password_new.min'          => 'Mật khẩu mới phải có ít nhất :min kí tự',
            'password_new.max'          => 'Mật khẩu mới không được vượt quá :max kí tự',
            'password_new.regex'        => 'Mật khẩu mới không hợp lệ',
            'password_res.required'     => 'Mật khẩu mới nhập lại không được để trống',
            'password_res.string'       => 'Mật khẩu mới nhập lại không phải là chuỗi',
            'password_res.min'          => 'Mật khẩu mới nhập lại phải có ít nhất :min kí tự',
            'password_res.max'          => 'Mật khẩu mới nhập lại không được vượt quá :max kí tự',
            'password_res.regex'        => 'Mật khẩu mới nhập lại không hợp lệ',
            'password_res.same'         => 'Mật khẩu mới nhập lại không khớp'
        ]);
        if($validation->passes()){
            $user = $this->getUserData();
            $password_old = $request->get('password_old');
            $password_new = $request->get('password_new');
            $password_res = $request->get('password_res');
            try{
                $password = DB::table('users')->where('id',$user['id'])->value('password');
                if(Hash::check($password_old, $password)){
                    $password = Hash::make($password_new);
                    DB::table('users')
                    ->where('id',$user['id'])
                    ->update([
                        'password' => $password
                    ]);
                    return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$request->all());
                }
                return $this->response->fail(ApiResponse::MESSAGE_FAILED,['message'=>'Mật khẩu cũ không chính xác']);
            }
            catch(Exception $exception){
                return $this->response->fail(ApiResponse::MESSAGE_FAILED);
            }
        }
        return $this->response->fail(ApiResponse::MESSAGE_FAILED,$validation->errors()->all());
    }
}
