<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use DB;
use Validator;

class accountController extends ControllerBase
{
    public function show(){
        $userData = $this->getUserData();
        $data = DB::table('account')
        ->select('account_value')
        ->where('account_user_id',$userData['id'])
        ->get();
        if(!empty($data)){
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS,$data);
        }
        return $this->response->fail(ApiResponse::MESSAGE_FAILED,['message'=>'ID không tồn tại']);
    }

    public function update(Request $request){
        $validation = Validator::make($request->all(),[
            'value' => 'required|numeric|min:1'
        ],[
            'value.required'    => 'Số tiền không được để trống',
            'value.numeric'     => 'Số tiền không phải là số',
            'value.min'         => 'Số tiền không được nhỏ hơn :min'
        ]);
        if($validation->passes()){
            $userData       = $this->getUserData();
            $value          = $request->get('value');
            DB::table('account')
            ->where('account_user_id',$userData['id'])
            ->update(['account_value' => $value]);
            return $this->response->success('Cập nhật thành công');
        }
        return $this->response->success($validation->errors()->all());
    }
}
