<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use DB;
use Validator;

class payoutController extends ControllerBase
{
    public function index(){
        $userData = $this->getUserData();
        try{
            $data = DB::table('payout')
            ->select('payout_id','payout_order_id','payout_value','payout_status')
            ->where('payout_user_id',$userData['id'])
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        catch(Exception $exception){
            return $this->response->fail('ID không tồn tại');
        }
    }

    public function create(Request $request){
        $validation = Validator::make($request->all(), [
            'order_id'     => 'required|numeric|exists:order_hotel,id',
            'value'        => 'required|numeric|min:1'
        ], [
            'order_id.required'         => 'ID hóa đơn không được để trống',
            'order_id.numeric'          => 'ID hóa đơn không phải là số',
            'value.numeric'             => 'Số tiền nạp vào không phải là số',
            'value.min'                 => 'Số tiền nạp vào phải lớn hơn :min'
        ]);
        if($validation->passes()){
            $userData   = $this->getUserData();
            $order_id   = $request->get('order_id');
            $value      = $request->get('value');
            DB::table('payout')->insert([
                'payout_user_id'        => $userData['id'],
                'payout_order_id'       => $order_id,
                'payout_value'          => $value
            ]);
            $valueAcc   = DB::table('account')
            ->where('account_user_id',$userData['id'])
            ->value('account_value');
            $account_value = $valueAcc-$value;
            if($account_value>0){
                DB::table('account')
                ->where('account_user_id',$userData['id'])
                ->update(['account_value' => $account_value]);
                return $this->response->success('Thanh toán thành công');
            }
            return $this->response->fail('Tài khoản không đủ để thanh toán');
        }
        return $this->response->fail($validation->errors()->all());
    }
}
