<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use DB;

class restaurantController extends ControllerBase
{
    public function index(){
        $data = DB::table('restaurant')
        ->select('restaurant_id','restaurant_image','restaurant_name','restaurant_address')
        ->where('restaurant_status',1)
        ->orderBy('restaurant_created','DESC')
        ->get();
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
    }

    public function show($id)
    {
        if(DB::table('restaurant')->where('restaurant_id',$id)->exists()){
            $data = DB::table('restaurant')
            ->select('restaurant_id','restaurant_image','restaurant_name','restaurant_address')
            ->where([['restaurant_id',$id],['restaurant_status',1]])
            ->orderBy('restaurant_created','DESC')
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        return $this->response->fail('ID không tồn tại');
    }

    public function food($id){
        if(DB::table('restaurant')->where('restaurant_id',$id)->exists()){
            $data = DB::table('food')
            ->select('food_id','food_image','food_name','food_amount','food_price')
            ->where([['food_restaurant_id',$id],['food_status',1]])
            ->orderBy('food_created','DESC')
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        return $this->response->fail('ID không tồn tại');
    }
}
