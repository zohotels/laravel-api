<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use DB;
use Validator;

class dishController extends ControllerBase
{
    public function index(){
        $data = DB::table('dish')
        ->select('dish_id','dish_name')
        ->where('dish_status',1)
        ->orderBy('dish_created','DESC')
        ->get();
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
    }

    public function show($id){
        if(DB::table('dish')->where('dish_id',$id)->exists()){
            $data = DB::table('restaurant')
            ->select('restaurant_id','restaurant_image','restaurant_name','restaurant_address')
            ->where([['restaurant_dish_id',$id],['restaurant_status',1]])
            ->orderBy('restaurant_created','DESC')
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        return $this->response->fail('ID không tồn tại');
    }
}
