<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Library\ApiResponse;
use Illuminate\Http\Request;
use DB;

class imageController extends Controller
{
    /**
     * Lấy danh sách ảnh theo khách sạn
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function hotel($id){
        try{
            $data = DB::table('image')
            ->where([['image_hotel_id',$id],['image_status',1]])
            ->orderBy('image_created','DESC')
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        catch(Exception $exception){
            return $this->response->fail(ApiResponse::MESSAGE_FAILED, ['message'=>'ID không tồn tại']);
        }
    }
}
