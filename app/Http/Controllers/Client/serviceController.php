<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use DB;

class serviceController extends ControllerBase
{
    public function index(){
        $data = DB::table('service')
        ->select('service_id','service_image','service_name')
        ->where('service_status',1)
        ->orderBy('service_created','DESC')
        ->get();
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
    }

    public function show($id)
    {
        if(DB::table('service')->where('service_id',$id)->exists(0)){
            $data = DB::table('service')
            ->select('service_id','service_image','service_name','service_description')
            ->where([['service_status',1],['service_id',$id]])
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        return $this->response->fail('ID không tồn tại');
    }
}
