<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ControllerBase;
use App\Http\Library\ApiResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use DB;

class surchargeController extends ControllerBase
{
    public function index()
    {
        $data = DB::table('surcharge')
        ->select('surcharge_id','surcharge_image','surcharge_name','surcharge_price')
        ->where('surcharge_status',1)
        ->orderBy('surcharge_created','DESC')
        ->get();
        return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
    }

    public function show($id)
    {
        if(DB::table('surcharge')->where('surcharge_id',$id)->exists()){
            $data = DB::table('surcharge')
            ->select('surcharge_id','surcharge_image','surcharge_name','surcharge_price')
            ->where([['surcharge_status',1],['surcharge_id',$id]])
            ->get();
            return $this->response->success(ApiResponse::MESSAGE_SUCCESS, $data);
        }
        return $this->response->fail('ID không tồn tại');
    }
}
