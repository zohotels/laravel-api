<?php

namespace App\Http\Library;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\Validation\Validator;

class ApiResponse
{
    const STATUS_SUCCESS = 1;
    const STATUS_FAILED = 0;

    const MESSAGE_SUCCESS = "Success";
    const MESSAGE_FAILED = "Fail";

    /**
     * HTTP Status code
     *
     * @var int
     */
    protected $statusCode = 200;

    protected $headers = [];

    /**
     */
    public function __construct()
    {
    }

    /**
     * Getter for statusCode
     *
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Setter for status code
     *
     * @param int $statusCode
     * @return ApiResponse
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * Setter for status code
     *
     * @param $headers
     * @return ApiResponse
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * Respond data with format is json
     *
     * @param $status
     * @param $message
     * @param $data
     * @param array $headers
     * @param null $optional
     * @return ResponseFactory
     */
    public function respond($status, $message, $data = null, array $headers = [], $optional = null)
    {
        $meta = [
            'status' => $status,
            'message' => $message,
            'data' => $data,
            'optional' => $optional,
        ];
        $content = $meta;

        if (!empty($this->headers)) {
            $headers = array_merge($this->headers);
        }

        return response()->json($content, $this->statusCode, $headers);
    }

    /**
     * Respond data with format is json
     *
     * @param $data
     * @param array $headers
     * @return ResponseFactory
     */
    public function respondV1($data, array $headers = [])
    {
        $content = [];
        if (!empty($data)) {
            $content = $data;
        }

        if (!empty($this->headers)) {
            $headers = array_merge($this->headers, $headers);
        }

        return response()->json($content, $this->statusCode, $headers);
    }

    /**
     * Response for message
     *
     * @param $message
     * @param string $data
     * @param int $code
     * @return ResponseFactory
     */
    public function withMessage($message, $data = '', $code = 0)
    {
        return $this->respond($code, $message, $data);
    }

    /**
     * Response for data
     *
     * @param string $message
     * @return ResponseFactory
     */
    public function withCreated($message = null)
    {
        if ($message == null) {
            $message = __('messages.message0166_CREATED_SUCCESSFULLY');
        }
        return $this->setStatusCode(201)->respond(0, $message, '');
    }

    /**
     * Generates a response with a 403 HTTP header and a given message.
     *
     * @param string $message
     * @return mixed
     */
    public function errorForbidden($message = null)
    {
        if ($message === null) {
            $message = __('messages.message0173_FORBIDDEN');
        }
        return $this->setStatusCode(403)->withMessage($message, '', 403);
    }

    /**
     * Generates a response with a 500 HTTP header and a given message.
     *
     * @param string $message
     * @return mixed
     */
    public function errorInternalError($message = null)
    {
        if ($message === null) {
            $message = __('messages.message0171_INTERNAL_ERROR');
        }
        return $this->setStatusCode(500)->withMessage($message, '', 500);
    }

    /**
     * Generates a response with a 404 HTTP header and a given message.
     *
     * @param string $message
     * @return mixed
     */
    public function errorNotFound($message = null)
    {
        if ($message === null) {
            $message = __('messages.message0169_RESOURCE_NOT_FOUND');
        }
        return $this->setStatusCode(404)->withMessage($message, '', 404);
    }

    /**
     * Generates a response with a 401 HTTP header and a given message.
     *
     * @param string $message
     * @return mixed
     */
    public function errorUnauthorized($message = null)
    {
        if ($message === null) {
            $message = __('messages.message0168_UNAUTHORIZED');
        }
        return $this->setStatusCode(401)->withMessage($message, '', 401);
    }

    /**
     * Generates a response with a 400 HTTP header and a given message.
     *
     * @param string $message
     * @return mixed
     */
    public function errorWrongArgs($message = null)
    {
        if ($message === null) {
            $message = __('messages.message0167_WRONG_ARGUMENTS');
        }
        return $this->setStatusCode(400)->withMessage($message, '', 400);
    }

    /**
     * Generates a Response with a 400 HTTP header and a given message from validator
     *
     * @param Validator $validator
     * @return ResponseFactory
     */
    public function errorWrongArgsValidator(Validator $validator)
    {
        $message = $validator->getMessageBag()->first();
        return $this->errorWrongArgs($message);
    }

    /**
     * Generates a response with a 410 HTTP header and a given message.
     *
     * @param string $message
     * @return mixed
     */
    public function errorGone($message = null)
    {
        if ($message === null) {
//            $message = __('messages.message0172_RESOURCE_NO_LONGER_AVAILABLE');
            $message = "Resource is no longer available";
        }
        return $this->setStatusCode(410)->withMessage($message);
    }

    /**
     * Generates a response with a 405 HTTP header and a given message.
     *
     * @param string $message
     * @return mixed
     */
    public function errorMethodNotAllowed($message = null)
    {
        if ($message === null) {
            $message = "Unsupported Method";
        }
        return $this->setStatusCode(405)->withMessage($message);
    }

    /**
     * Generates a Response with a 431 HTTP header and a given message.
     *
     * @param string $message
     * @return mixed
     */
    public function errorUnwillingToProcess($message = null)
    {
        if ($message === null) {
            $message = __('messages.message0170_SERVER_IS_UNWILLING_TO_PROCESS_THE_REQUEST');
        }
        return $this->setStatusCode(431)->withMessage($message);
    }

    public function success($message = self::MESSAGE_SUCCESS, $data = null, $option = null)
    {
        return $this->respond(self::STATUS_SUCCESS, $message, $data, [], $option);
    }

    public function fail($message = self::MESSAGE_FAILED, $data = null, $option = null)
    {
        return $this->respond(self::STATUS_FAILED, $message, $data, [], $option);
    }
}
