<?php

namespace App\Http\Library;

use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use Exception;
use UnexpectedValueException;

trait JwtExt
{
    /**
     * @param $jwt
     * @param bool $array
     * @return mixed|object
     * @throws Exception
     */
    function decodeJwt($jwt, $array = false)
    {
        $jwtKey = config('jwt.secret');
        $algo = config('jwt.algo');
        try {
            $data = JWT::decode($jwt, $jwtKey, [$algo]);
            return $array ? json_decode(json_encode($data), true) : $data;
        } catch (ExpiredException $e) {
            throw new Exception("Phiên đăng nhập đã hết hạn.", 999);
        } catch (SignatureInvalidException | UnexpectedValueException | BeforeValidException $e) {
            throw new Exception("Không tìm thấy tài khoản.", 999);
        }
    }

    /**
     * @param array $data
     * @return string
     */
    function encodeJwt(array $data)
    {
        $jwtKey = config('jwt.secret');
        $algo = config('jwt.algo');
        $url = config('app.url');
        $principal['iat'] = time();
        $principal['iss'] = $url;
        $principal['aud'] = $url;
        $principal['exp'] = time() + intval(config('jwt.exp_in'));
        $principal['data'] = $data;
        return JWT::encode($principal, $jwtKey, $algo);
    }
}
