<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class HotelCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cityID' => 'required|integer|exists:city,id',
            'image' => 'required|image|max:5000',
            'name' => 'required|unique:hotel,name|string|min:1|max:255',
            'address' => 'required|string|max:255',
            'policy' => 'nullable|string',
            'price' => 'nullable|numeric|digits_between:1,20',
            'promotion' => 'nullable|numeric|digits_between:1,4',
        ];
    }

    public function messages()
    {
        return [
            'cityID.required' => 'ID thành phố không được để trống',
            'cityID.integer' => 'ID thành phố phải là số nguyên',
            'cityID.exists' => 'ID thành phố không tồn tại',

            'image.required' => 'Ảnh không được để trống',
            'image.image' => 'Ảnh không đúng định dạng',
            'image.max' => 'Ảnh không được vượt quá :max kB',

            'name.required' => 'Tên không được để trống',
            'name.unique' => 'Tên đã tồn tại',
            'name.string' => 'Tên không phải là chuỗi',
            'name.min' => 'Tên phải có ít nhất :min kí tự',
            'name.max' => 'Tên không được vượt quá 255 kí tự',

            'address.required' => 'Địa chỉ không được để trống',
            'address.string' => 'Địa chỉ không phải là chuỗi',
            'address.max' => 'Địa chỉ không được vượt quá :max kí tự',

            'policy.string' => 'Chính sách không phải là chuỗi',

            'price.numeric' => 'Giá không phải là số',
            'price.digits_between' => 'Giá không được vượt quá :max số',

            'promotion.numeric' => '% khuyến mại không phải là số',
            'promotion.digits_between' => '% khuyến mại không được vượt quá :max chữ số',

            'favorite.numeric' => 'Số lượt yêu thích không phải là số'
        ];
    }
}
