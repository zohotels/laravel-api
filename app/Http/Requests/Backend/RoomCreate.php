<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class RoomCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hotelID'       => 'required|integer|exists:hotel,id',
            'image'         => 'required|image|max:5000',
            'name'          => 'required|string|max:255|unique:room,name',
            'description'   => 'nullable|string',
            'amount'        => 'nullable|integer|min:0',
            'bed'           => 'nullable|integer|min:0',
            'typeID'        => 'required|integer|exists:bedtype,id',
            'price'         => 'nullable|numeric|digits_between:1,20',
            'promotion'     => 'nullable|numeric|digits_between:1,4',
        ];
    }

    public function messages()
    {
        return [
            'hotelID.required'          => 'ID khách sạn không được để trống',
            'hotelID.integer'           => 'ID khách sạn phải là số nguyên',
            'hotelID.exists'            => 'ID khách sạn không tồn tại',

            'image.required'            => 'Ảnh không được để trống',
            'image.image'               => 'Ảnh không đúng định dạng',
            'image.max'                 => 'Ảnh không được vượt quá :max kB',

            'name.required'             => 'Tên không được để trống',
            'name.string'               => 'Tên không phải là chuỗi',
            'name.max'                  => 'Tên không được vượt quá :max kí tự',
            'name.unique'               => 'Tên đã tồn tại',

            'description.string'        => 'Mô tả không phải là chuỗi',

            'amount.integer'            => 'Số lượng không phải là số nguyên',
            'amount.min'                => 'Số lượng thấp nhất là :min',

            'bed.integer'              => 'Số giường không phải là số nguyên',
            'bed.min'                   => 'Số giường thấp nhất là :min',

            'typeID.required'           => 'ID loại giường không được để trống',
            'typeID.integer'            => 'ID loại giường phải là số nguyên',
            'typeID.exists'             => 'ID loại giường không tồn tại',

            'price.numeric'             => 'Giá không phải là số',
            'price.digits_between'      => 'Giá không được vượt quá :max số',

            'promotion.numeric'         => '% khuyến mại không phải là số',
            'promotion.digits_between'  => '% khuyến mại không được vượt quá :max chữ số',

            'favorite.numeric'          => 'Số lượt yêu thích không phải là số'
        ];
    }
}
