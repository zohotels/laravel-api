<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class ImageCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hotelID' => 'required|integer|exists:hotel,id',
            'image' => 'required|array|size:4',
            'image.*' => 'image|max:5000'
        ];
    }

    public function messages()
    {
        return [
            'hotelID.required' => 'ID khách sạn không được đẻ trống',
            'hotelID.integer' => 'ID khách sạn phải là số nguyên',
            'hotelID.exists' => 'ID khách sạn không tồn tại',

            'image.required' => 'Mảng hình ảnh không được để trống',
            'image.size' => 'Mảnh hình ảnh không được vượt quá :size phần tử',
            'image.array' => 'Mảng hình ảnh không hợp lệ',

            'image.*.image' => 'Ảnh không đúng định dạng',
            'image.*.max' => 'Ảnh không được vượt quá :max kB'
        ];
    }
}
