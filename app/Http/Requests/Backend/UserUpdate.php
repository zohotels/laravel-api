<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'              => 'required|exists:users,code',
            'fullname'          => 'required|string',
            'phone'             => 'required|numeric|unique:users,phone',
            'avatar'            => 'nullable|image|max:5000',
            'email'             => 'nullable|email|max:255',
            'address'           => 'nullable|string|max:255',
            'introduce_code'    => 'nullable|exists:users,phone',
            'type'              => 'required|in:client,admin'    
        ];
    }

    public function messages(){
        return [
            'code.required'         => 'Mã người dùng không được để trống',
            'code.exists'           => 'Mã người dùng không tồn tại',

            'fullname.required'     => 'Họ và tên không được để trống',
            'fullname.string'       => 'Họ và tên không phải là chuỗi',

            'phone.required'        => 'Số điện thoại không được để trống',
            'phone.numeric'         => 'Số điện thoại không hợp lệ',
            'phone.unique'          => 'Số điện thoại đã tồn tại',

            'password.required'     => 'Mật khẩu không được để trống',
            'password.string'       => 'Mật khẩu không phải là chuỗi',
            'password.max'          => 'Mật khẩu không được vượt quá :max kí tự',
            'password.min'          => 'Mật khẩu phải có ít nhất :min kí tự',
            'password.regex'        => 'Mật khẩu không đúng định dạng',

            'password_re.required'  => 'Mật khẩu nhập lại không được để trống',
            'password_re.same'      => 'Mật khẩu nhập lại không khớp',

            'avatar.image'          => 'Ảnh đại diện không đúng định dạng',
            'avatar.max'            => 'Ảnh đại điện không được vượt quá :max kB',

            'email.email'           => 'Email không đúng định dạng',
            'email.max'             => 'Email không được vượt qua :max kí tự',

            'address.string'        => 'Địa chỉ không phải là chuỗi',
            'address.max'           => 'Địa chỉ không được vượt quá :max kí tự',

            'introduce_code.exists' => 'Mã giới thiệu không tồn tại',

            'type.required'         => 'Chưa chọn quyền của người dùng',
            'type.in'               => 'Quyền người dùng không hợp lệ (Vd: :values)'
        ];
    }
}
