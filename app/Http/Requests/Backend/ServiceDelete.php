<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class ServiceDelete extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|array',
            'id.*' => 'string|exists:service,id'
        ];
    }

    public function messages()
    {
        return [
            'id.required' => 'Mảng ID dịnh vụ không được để trống',
            'id.array' => 'Mảng ID dịnh vụ không hợp lệ',

            'id.*.exists' => 'ID dịnh vụ không tồn tại'
        ];
    }
}
