<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class HotelDelete extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|array',
            'code.*' => 'string|exists:hotel,code'
        ];
    }

    public function messages()
    {
        return [
            'code.required' => 'Mảng mã khách sạn không được để trống',
            'code.array' => 'Mảng mã khách sạn không hợp lệ',

            'code.*.exists' => 'Mã khách sạn không tồn tại'
        ];
    }
}
