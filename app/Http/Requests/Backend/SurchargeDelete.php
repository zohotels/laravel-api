<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class SurchargeDelete extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|array',
            'code.*' => 'string|exists:surcharge,code'
        ];
    }

    public function messages()
    {
        return [
            'code.required' => 'Mảng mã dịnh vụ mất phí không được để trống',
            'code.array' => 'Mảng mã dịnh vụ mất phí không hợp lệ',

            'code.*.exists' => 'Mã dịnh vụ mất phí không tồn tại'
        ];
    }
}
