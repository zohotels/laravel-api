<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class ImageDelete extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|array',
            'id.*' => 'string|exists:image,id'
        ];
    }

    public function messages()
    {
        return [
            'id.required' => 'Mảng ID dịnh vụ miễn phí không được để trống',
            'id.array' => 'Mảng ID dịnh vụ miễn phí không hợp lệ',

            'id.*.exists' => 'ID dịnh vụ miễn phí không tồn tại'
        ];
    }
}
