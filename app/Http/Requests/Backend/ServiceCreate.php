<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class ServiceCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|image|max:5000',
            'name' => 'required|string|max:255|unique:service,name',
            'description' => 'nullable|string'
        ];
    }

    public function messages()
    {
        return [
            'image.required' => 'Ảnh không được để trống',
            'image.image' => 'Ảnh khống đúng định dạng',
            'image.max' => 'Ảnh không được vượt quá :max kB',

            'name.required' => 'Tên không được để trống',
            'name.string' => 'Tên không phải là chuỗi',
            'name.max' => 'Tên không được vượt quá :max kí tự',
            'name.unique' => 'Tên đã tồn tại',

            'description.string' => 'Mô tả không phải là chuỗi'
        ];
    }
}
