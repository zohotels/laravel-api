<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class UserDelete extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|array',
            'code.*' => 'string|exists:users,code'
        ];
    }

    public function messages()
    {
        return [
            'code.required' => 'Mảng mã người dùng không được để trống',
            'code.array' => 'Mảng mã người dùng không hợp lệ',

            'code.*.exists' => 'Mã người dùng không tồn tại'
        ];
    }
}
