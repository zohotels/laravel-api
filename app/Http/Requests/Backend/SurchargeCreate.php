<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class SurchargeCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hotelID'   => 'required|integer|exists:hotel,id',
            'image'     => 'required|image|max:5000',
            'name'      => 'required|string|max:255|unique:surcharge,name',
            'price'     => 'required|numeric|digits_between:1,20',
        ];
    }

    public function messages()
    {
        return [
            'hotelID.required' => 'ID thành phố không được để trống',
            'hotelID.integer'  => 'ID thành phố phải là số nguyên',
            'hotelID.exists'   => 'ID thành phố không tồn tại',

            'image.required'    => 'Ảnh không được để trống',
            'image.max' => 'Ảnh không được vượt quá :max kB',
            'image.image' => 'Ảnh không đúng định dạng',

            'name.required' => 'Tên không được để trống',
            'name.string' => 'Tên không phải là chuỗi',
            'name.max' => 'Tên không được vượt quá :max kí tự',
            'name.unique' => 'Tên đã tồn tại',

            'price.required' => 'Giá không được để trống',
            'price.numeric' => 'Giá không phải là số',
            'price.digits_between' => 'Giá không được vượt quá :max chữ số'
        ];
    }
}
