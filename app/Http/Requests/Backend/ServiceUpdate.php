<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class ServiceUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'  => 'required|exists:service,id',
            'image' => 'nullable|image|max:255',
            'name'  => 'required|string|max:255|unique:service,name',
            'description' => 'nullable|string'
        ];
    }

    /**
     * @return array|string[]
     */
    public function messages()
    {
        return [
            'id.required' => 'ID không được để trống ',
            'id.exists' => 'ID không tồn tại',

            'image.image' => 'Ảnh không đúng định dạng',
            'image.max' => 'Ảnh không được vượt quá :max',

            'name.required' => 'Tên không được để trống',
            'name.string' => 'Tên không phải là chuỗi',
            'name.max'  => 'Tên không được vượt quá :max kí tự',
            'name.unique' => 'Tên đã tồn tại',

            'description.string' => 'Mô tả không phải là chuỗi'
        ];
    }
}
