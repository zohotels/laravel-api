<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class TableList extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page'      => 'nullable|integer|min:0',
            'limit'     => 'nullable|integer|min:5|max:100'
        ];
    }

    /**
     * @return array|string[]
     */
    public function messages()
    {
        return [
            'page.required'     => 'Số trang không được để trống',
            'page.integer'      => 'Số trang phải là số nguyên',
            'page.min'          => 'Số trang thấp nhất là :min',

            'limit.integer'     => 'Số bản ghi trên một trang phải là số nguyên',
            'limit.min'         => 'Số bản ghi trên một trang thấp nhất là :min',
            'limit.max'         => 'Số bản ghi trên một trang tối đa là :max'
        ];
    }
}
