<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class RoomDelete extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'      => 'required|array',
            'code.*'    => 'string|exists:room,code'
        ];
    }

    public function messages(){
        return [
            'code.required'         => 'Mảng mã phòng không được để trống',
            'code.array'            => 'Mảng mã phòng  không hợp lệ',

            'code.*.exists'         => 'Mã phòng không tồn tại'
        ];
    }
}
