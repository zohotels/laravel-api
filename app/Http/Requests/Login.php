<?php

namespace App\Http\Requests;

use App\Models\BaseModel;
use Illuminate\Foundation\Http\FormRequest;

class Login extends FormRequest
{
    public function rules()
    {
        $password = BaseModel::PASSWORD;
        return [
            'phone' => 'required|numeric',
            'password' => "required|string|max:{$password['max']}|min:{$password['min']}|regex:/^\S+$/"
        ];
    }

    /**
     * @return array|string[]
     */
    public function messages()
    {
        return [
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.numeric' => 'Số điện thoại không hợp lệ',

            'password.required' => 'Mật khẩu không được để trống',
            'password.string' => 'Mật khẩu phải là dạng chuỗi',
            'password.max' => 'Mật khẩu không dài hơn :max ký tự',
            'password.min' => 'Mật khẩu không ít hơn :min ký tự',
            'password.regex' => 'Mật khẩu không chứa dấu khoảng trắng'
        ];
    }
}
