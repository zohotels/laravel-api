<?php

namespace App\Models;

use App\Http\Library\JwtExt;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    const TYPE_ADMIN = 'admin';
    const TYPE_CLIENT = 'client';
    const LINK_IMAGE = 'upload/user';

    use SoftDeletes;
    use JwtExt;

    protected $table = 'users';

    protected $fillable = [
        'code', 'name', 'email', 'password', 'address', 'phone', 'type',
    ];

    protected $hidden = [
        'password',
    ];

    /**
     * @param $bodyData
     * @param $typeUser
     * @return array
     */
    protected function userLogin($bodyData, $typeUser)
    {
        $user = self::findUserByType('phone', $bodyData['phone'], $typeUser);
        if (empty($user)) {
            return [
                'status' => 0,
                'message' => 'Không tìm thấy người dùng'
            ];
        }
        if ($user->deleted_at !== null && $user->deleted_at !== "") {
            return [
                'status' => 0,
                'message' => 'Tài khoản bị khóa'
            ];
        }
        if (!Hash::check($bodyData['password'], $user->password)) {
            return [
                'status' => 0,
                'message' => 'Mật khẩu không chính xác'
            ];
        }
        if ($user['type'] !== $typeUser) {
            return [
                'status' => 0,
                'message' => 'Không có quyền truy cập hệ thống'
            ];
        }
        try {
            $userInfo = self::detailProfile($user['code']);
            $dataUser = $userInfo->makeHidden(['password'])->toArray();
            $dataUser['token'] = $this->encodeJwt($dataUser);
            $tokenMd5 = md5($dataUser['token']);
            $userId = $dataUser['code'];
            $dataUser['last_login'] = time();
            Cache::put("user_info_login:{$userId}:{$tokenMd5}", $dataUser);
            return [
                'status' => 1,
                'data' => $dataUser
            ];
        } catch (Exception $exception) {
            return [
                'status' => 0,
                'message' => 'Đăng nhập không thành công'
            ];
        }
    }

    /**
     * @param $field
     * @param $data
     * @param null $type
     * @return User|\Illuminate\Database\Eloquent\Builder|Model|Builder|object|null
     */
    protected function findUserByType($field, $data, $type = null)
    {
        if (empty($type)) {
            $user = self::withTrashed()->where($field, $data)->first();
        } else {
            $user = self::withTrashed()->where([
                [$field, $data],
                ['type', $type]
            ])->first();
        }
        if (empty($user)) {
            return null;
        }
        return $user;
    }

    /**
     * @param $userCode
     * @return User|\Illuminate\Database\Eloquent\Builder|Model|Builder|object|null
     */
    protected function detailProfile($userCode)
    {
        $user = self::withTrashed()->where('code', $userCode)->first();
        if (!empty($user->avatar) && file_exists(public_path(self::LINK_IMAGE . '/' . $userCode . '/' . $user->avatar))) {
            $user['link_avatar'] = asset(self::LINK_IMAGE . '/' . $userCode . '/' . $user->avatar);
        } else {
            $user['link_avatar'] = asset(BaseModel::DEFAULT_AVATAR);
        }
        return $user;
    }
}
