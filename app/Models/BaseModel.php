<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    const LENGTH_CODE = 15;
    const PAGINATE = 20;
    const PASSWORD = [
        'min' => 6,
        'max' => 255
    ];
    const imageUserDefault = 10;
    const DEFAULT_AVATAR = 'upload/image/default-avatar.jpg';
    const DEFAULT_IMAGE = 'upload/image/default-image.jpg';
}
