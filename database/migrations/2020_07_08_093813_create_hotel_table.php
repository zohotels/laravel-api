<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\BaseModel;

class CreateHotelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel', function (Blueprint $table) {
            $table->increments('id');
            $table->char('code',20);
            $table->integer('cityID');
            $table->string('image');
            $table->string('name');
            $table->string('address');
            $table->text('policy')->nullable();
            $table->char('price',20)->default(0);
            $table->char('promotion',5)->default(0);
            $table->integer('favorite')->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });

        DB::table('hotel')->insert([
            'code'      => Str::random(BaseModel::LENGTH_CODE),
            'cityID'    => '1',
            'image'     => 'hanoi.jpg',
            'name'      => 'Zhotels Yên Phụ, Tây hồ, hà Nội',
            'address'   => 'Số 213 Phạm Tuấn Tài, Yên Phụ, Tây Hồ, hà Nội',
            'policy'    => 'Chính sách',
            'price'     => '253000',
            'promotion' => '15',
            'favorite'  => '0'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel');
    }
}
