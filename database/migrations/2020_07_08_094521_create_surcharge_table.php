<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\BaseModel;

class CreateSurchargeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surcharge', function (Blueprint $table) {
            $table->increments('id');
            $table->char('code',20);
            $table->integer('hotelID');
            $table->string('image');
            $table->string('name');
            $table->char('price',20);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });

        DB::table('surcharge')->insert([
           [
               'code' => Str::random(BaseModel::LENGTH_CODE),
               'hotelID' => '1',
               'image' => 'surcharge1.jpg',
               'name' => 'surcharge 1',
               'price' => '153000'
           ],[
                'code' => Str::random(BaseModel::LENGTH_CODE),
                'hotelID' => '1',
                'image' => 'surcharge2.jpg',
                'name' => 'surcharge 2',
                'price' => '153000'
            ],[
                'code' => Str::random(BaseModel::LENGTH_CODE),
                'hotelID' => '1',
                'image' => 'surcharge3.jpg',
                'name' => 'surcharge 3',
                'price' => '153000'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surcharge');
    }
}
