<?php

use App\Models\BaseModel;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->char('code', 20);           
            $table->string('fullname');
            $table->char('phone', 20);
            $table->string('password');
            $table->string('avatar')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->char('introduce_code',20)->nullable();
            $table->char('type', 20);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });

        DB::table('users')->insert([
            [
                'code'      => Str::random(BaseModel::LENGTH_CODE),
                'phone'     => '0354525558',
                'fullname'  => 'Administrator',
                'email'     => 'admin@gmail.com',
                'password'  => Hash::make('password'),
                'type'      => User::TYPE_ADMIN
            ], [
                'code' => Str::random(BaseModel::LENGTH_CODE),
                'phone'     => '03545255581',
                'fullname' => 'Account customer 1',
                'email' => 'customer1@gmail.com',
                'password' => Hash::make('password'),
                'type' => User::TYPE_CLIENT
            ], [
                'code' => Str::random(BaseModel::LENGTH_CODE),
                'phone'     => '03545255582',
                'fullname' => 'Account customer 2',
                'email' => 'customer2@gmail.com',
                'password' => Hash::make('password'),
                'type' => User::TYPE_CLIENT
            ], [
                'code' => Str::random(BaseModel::LENGTH_CODE),
                'phone'     => '03545255583',
                'fullname' => 'Account customer 3',
                'email' => 'customer3@gmail.com',
                'password' => Hash::make('password'),
                'type' => User::TYPE_CLIENT
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
