<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\BaseModel;

class CreateRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room', function (Blueprint $table) {
            $table->increments('id');
            $table->char('code',20);
            $table->integer('hotelID');
            $table->string('image');
            $table->string('name');
            $table->text('description')->nullable();
            $table->tinyInteger('amount')->default(0);
            $table->tinyInteger('bed')->default(0);
            $table->tinyInteger('typeID');
            $table->char('price',20)->default(0);
            $table->char('promotion',5)->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });

        DB::table('room')->insert([[
            'code'          => Str::random(BaseModel::LENGTH_CODE),
            'hotelID'       => '1',
            'image'         => '0123456789',
            'name'          => 'Phòng 1',
            'description'   => 'description',
            'amount'        => '10',
            'bed'           => '1',
            'typeID'        => '1',
        ],[
            'code'          => Str::random(BaseModel::LENGTH_CODE),
            'hotelID'       => '1',
            'image'         => '0123456789',
            'name'          => 'Phòng 1',
            'description'   => 'description',
            'amount'        => '10',
            'bed'           => '1',
            'typeID'        => '1',
        ]]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room');
    }
}
