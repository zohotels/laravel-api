<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderRoom', function (Blueprint $table) {
            $table->increments('id');
            $table->char('code',20);
            $table->smallInteger('userID');
            $table->smallInteger('roomID');
            $table->string('receiveTime');
            $table->string('returnTime');
            $table->tinyInteger('receiveEarly')->default(0);
            $table->tinyInteger('returnLate')->default(0);
            $table->tinyInteger('amount');
            $table->char('discountCode',20)->nullable();
            $table->tinyInteger('bookingFor');
            $table->string('email');
            $table->string('fullname');
            $table->char('phone',20);
            $table->text('notes')->nullable();
            $table->float('total');
            $table->tinyInteger('payment');
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('aspect')->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderRoom');
    }
}
