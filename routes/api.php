<?php

use Illuminate\Support\Facades\Route;

// Client
Route::name('client.')->namespace('Client')->group(function () {

    // Dish - loại món ăn
    Route::prefix('dish')->name('dish.')->group(function () {
        Route::get('', 'dishController@index')->name('index');
        Route::get('{id}/restaurant', 'dishController@show')->name('show');
    });
    // Restaurant - nhà hàng
    Route::prefix('restaurant')->name('restaurant.')->group(function () {
        Route::get('{id}', 'restaurantController@show')->name('show');
        Route::get('{id}/food', 'restaurantController@food')->name('food');
    });
    // City - thành phố - địa điểm
    Route::prefix('city')->name('city.')->group(function () {
        Route::get('', 'cityController@index')->name('index');
        Route::get('hightlight', 'cityController@hightlight')->name('hightlight');
        Route::get('{id}', 'cityController@show')->name('show');
        Route::get('{id}/hotel', 'cityController@hotel')->name('hotel');
    });
    // Hotel - khách sạn
    Route::prefix('hotel')->name('hotel.')->group(function () {
        Route::get('{id}', 'hotelController@show')->name('show');
        Route::get('{id}/room', 'hotelController@room')->name('room');
        Route::get('{id}/image', 'hotelController@image')->name('image');
        Route::get('{id}/feedback/count', 'feedbackController@hotelCount')->name('feedback.count');
        Route::get('{id}/feedback', 'feedbackController@hotel')->name('feedback');
    });
    // service - dịnh vụ
    Route::prefix('service')->name('service.')->group(function () {
        Route::get('', 'serviceController@index')->name('index');
        Route::get('{id}', 'serviceController@show')->name('show');
    });
    // surcharge - phụ phí
    Route::prefix('surcharge')->name('surcharge.')->group(function () {
        Route::get('', 'surchargeController@index')->name('index');
        Route::get('{id}', 'surchargeController@show')->name('show');
    });
    // Preferential - ưu đãi
    Route::prefix('preferential')->name('preferential.')->group(function () {
        Route::get('', 'preferentialController@index')->name('index');
        Route::get('{id}', 'preferentialController@show')->name('show');
    });
    // Authenticate
    Route::prefix('auth')->name('auth.')->group(function () {
        Route::post('login', 'AuthenticateController@login')->name('login');
        Route::post('register', 'AuthenticateController@register')->name('register');
        Route::post('forgot-password', 'AuthenticateController@forgot_password')->name('forget_password');
    });

    // Something
    Route::middleware('check-client-login')->group(function () {
        // something has login
        // Authenticate
        Route::prefix('auth')->name('auth.')->group(function () {
            Route::get('profile', 'AuthenticateController@show')->name('show');
            Route::post('profile', 'AuthenticateController@update')->name('update');
            Route::post('change-password', 'AuthenticateController@change_password')->name('change_password');
            Route::get('logout', 'AuthenticateController@logout')->name('logout');
        });
        // Favorite - yêu thích
        Route::prefix('favorite')->name('favorite')->group(function () {
            Route::get('', 'favoriteController@index')->name('index');
            Route::get('check/{id}', 'favoriteController@check')->name('check');
            Route::post('', 'favoriteController@create')->name('create');
            //Route::delete('{id}','favoriteController@delete')->name('delete');
        });
        // Feedback - Đánh giá
        Route::prefix('feedback')->name('feedback.')->group(function () {
            Route::post('', 'feedbackController@create')->name('create');
            Route::put('', 'feedbackController@update')->name('update');
            Route::delete('{id}', 'feedbackController@delete')->name('delete');
        });
        // Account - tài khoản
        Route::prefix('account')->name('account.')->group(function () {
            Route::get('', 'accountController@show')->name('show');
            Route::put('', 'accountController@update')->name('update');
        });
        // Payin - nạp tiền
        Route::prefix('payin')->name('payin.')->group(function () {
            Route::get('', 'payinController@index')->name('index');
            Route::post('', 'payinController@create')->name('create');
        });
        // Payout - Thanh toán
        Route::prefix('payout')->name('payout.')->group(function () {
            Route::get('', 'payoutController@index')->name('index');
            Route::post('', 'payoutController@create')->name('create');
        });
        // Order/hotel
        Route::prefix('order-hotel')->name('order-hotel.')->group(function () {
            Route::get('', 'orderHotelController@index')->name('index');
            Route::post('', 'orderHotelController@create')->name('create');
            Route::get('{id}', 'orderHotelController@show')->name('show');
            Route::put('{id}/status', 'orderHotelController@status')->name('status');
            Route::put('{id}/aspect', 'orderHotelController@aspect')->name('aspect');
            Route::get('{id}/room', 'orderHotelController@room_index')->name('room_index');
            Route::post('{id}/room', 'orderHotelController@room_create')->name('room_create');
            Route::get('{id}/surcharge', 'orderHotelController@surcharge_index')->name('surcharge_index');
            Route::post('{id}/surcharge', 'orderHotelController@surcharge_create')->name('surcharge_create');
        });

        // order/restaurant
        Route::prefix('order-restaurant')->name('order-restaurant.')->group(function () {
            Route::get('', 'orderRestaurantController@index')->name('index');
            Route::get('{id}', 'orderRestaurantController@show')->name('show');
            Route::get('{id}/food', 'orderRestaurantController@food_index')->name('food_index');
            Route::post('food', 'orderRestaurantController@food_create')->name('food_create');
            Route::post('', 'orderRestaurantController@create')->name('create');
        });
    });

});
// Backend
Route::prefix('admin')->name('backend.')->namespace('Backend')->group(function () {
    // Authenticate
    Route::prefix('auth')->name('auth.')->group(function () {
        Route::post('login', 'AuthenticateController@login')->name('login');
    });
    // Something
    Route::middleware('check-admin-login')->group(function () {
        // something has login
        Route::prefix('auth')->name('auth.')->group(function () {
            Route::get('logout', 'AuthenticateController@logout')->name('logout');
        });

        // user
        Route::prefix('user')->name('user.')->group(function () {
            Route::get('list', 'UserController@list')->name('list');
            Route::get('detail', 'UserController@detail')->name('detail');
            Route::post('create', 'UserController@create')->name('create');
            Route::post('update', 'UserController@update')->name('update');
            Route::delete('delete', 'UserController@delete')->name('delete');
            Route::delete('lock', 'UserController@lock')->name('lock');
        });

        // city
        Route::prefix('city')->name('city.')->group(function () {
            Route::get('list', 'CityController@list')->name('list');
            Route::get('detail', 'CityController@detail')->name('detail');
        });

        // hotel
        Route::prefix('hotel')->name('hotel.')->group(function () {
            Route::get('list', 'HotelController@list')->name('list');
            Route::get('detail', 'HotelController@detail')->name('detail');
            Route::post('create','HotelController@create')->name('create');
            Route::post('update','HotelController@update')->name('update');
            Route::delete('delete','HotelController@delete')->name('delete');
        });

        // Room
        Route::prefix('room')->name('room.')->group(function(){
            Route::get('list','RoomController@list')->name('list');
            Route::get('detail','RoomController@detail')->name('detail');
            Route::post('create','RoomController@create')->name('create');
            Route::post('update','RoomController@update')->name('update');
            Route::delete('delete','RoomController@delete')->name('delete');
        });

        // Service
        Route::prefix('service')->name('service.')->group(function(){
            Route::get('list','ServiceController@list')->name('list');
            Route::get('detail','ServiceController@detail')->name('detail');
            Route::post('create','ServiceController@create')->name('create');
            Route::post('update','ServiceController@update')->name('update');
            Route::delete('delete','ServiceController@delete')->name('delete');
        });

        // Surcharge
        Route::prefix('surcharge')->name('surcharge.')->group(function(){
            Route::get('list','SurchargeController@list')->name('list');
            Route::get('detail','SurchargeController@detail')->name('detail');
            Route::post('create','SurchargeController@create')->name('create');
            Route::post('update','SurchargeController@update')->name('update');
            Route::delete('delete','SurchargeController@delete')->name('delete');
        });

        // Image
        Route::prefix('image')->name('image.')->group(function(){
           Route::get('list','ImageController@list')->name('list');
           Route::get('detail','ImageController@detail')->name('detail');
           Route::post('create','ImageController@create')->name('create');
           Route::post('update','ImageController@update')->name('update');
           Route::delete('delete','ImageController@delete')->name('delete');
        });
    });
});
// End
